import logging
import os
import random

from bigtree import findall
from fitz import fitz, Pixmap

from unibsv.preprocess import detect_tables, read_pdf, NODE_TYPE_IMAGE
from unibsv.process import process_image


def visualize_images(file_path: str, output_dir: str):
    """
    Render images used for risk processing, with variants including annotations.
    :param file_path: PDF source file
    :param output_dir: where images should be written
    """
    pdf = read_pdf(file_path)
    for i, image in enumerate(findall(pdf, lambda n: n.is_typed(NODE_TYPE_IMAGE))):
        process_image(image, i, output_dir)


def visualize_blocks(file_path: str, output_dir: str) -> list[str]:
    """
    Render PDF pages as images with highlighted blocks, lines and drawings, to provide a way to understand page layouts.
    :param file_path: PDF source file
    :param output_dir: where images should be written
    :return: the paths of the generated images
    """
    images = []
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    with fitz.open(file_path) as doc:
        for page in doc:  # iterate through the pages
            pix: Pixmap = page.get_pixmap()  # render page to an image

            # drawings
            page_drawings = page.get_drawings()
            for i in range(len(page_drawings)):
                color = (random.randint(100, 250), random.randint(100, 250), random.randint(100, 250))
                bbox = page_drawings[i]["rect"]
                draw_bbox(pix, bbox, color, fill=True)

            # blocks & lines
            page_blocks = page.get_text("dict", sort=True)["blocks"]
            for i in range(len(page_blocks)):
                block = page_blocks[i]
                color = (100 + int(i * 155 / len(page_blocks)), 50, 50)
                draw_bbox(pix, block["bbox"], color)
                if "lines" in block:
                    for line_idx, line in enumerate(block["lines"]):
                        draw_bbox(pix, line["bbox"], (50, 50, 100 + int(i * 155 / len(page_blocks))), 2)

            # resolved tables
            tables = detect_tables(page)
            logging.debug(f"resolved {len(tables)} table(s) for page {page.number}")
            for i in range(len(tables)):
                color = (50, 50, 100 + int(i * 155 / len(tables)))
                for line in tables[i]:
                    for cell in line:
                        draw_bbox(pix, cell, color, 7)

            output_image = os.path.join(output_dir, f"{os.path.basename(doc.name)}-{page.number}.png")
            pix.save(output_image)  # store image as a PNG
            images.append(output_image)
    return images


def draw_bbox(pix, bbox, color, dashed: int = 0, fill: bool = False):
    """
    Draw a bounding box of the given color.
    :param pix: the Pixmap to draw to
    :param bbox: the bounding box to draw
    :param color: the color to use
    :param dashed: the length of dashes, if any
    :param fill: if an inner margin of the box should be colored
    """
    # clip the given box to the pixmap dimensions
    x0 = max(0, int(min(bbox[0], bbox[2])))
    y0 = max(0, int(min(bbox[1], bbox[3])))
    x1 = min(pix.width - 1, int(max(bbox[0], bbox[2])))
    y1 = min(pix.height - 1, int(max(bbox[1], bbox[3])))
    i = 0
    # draw horizontal lines
    for x in range(x0, x1):
        if dashed > 0:
            i += 1
            if int(i / dashed) % 2 == 1:
                continue
        pix.set_pixel(x, y0, color)
        pix.set_pixel(x, y1, color)
    # draw vertical lines
    for y in range(y0, y1):
        if dashed > 0:
            i += 1
            if int(i / dashed) % 2 == 1:
                continue
        pix.set_pixel(x0, y, color)
        pix.set_pixel(x1, y, color)

    # fill an inner margin of 1 to 4 pixels wide
    if fill and x1 - x0 > 2 and y1 - y0 > 2:
        for i in range(1, min(4, int((x1 - x0) / 2))):
            for j in range(1, min(4, int((y1 - y0) / 2))):
                draw_bbox(pix, (x0 + i, y0 + j, x1 - i, y1 - j), color, dashed, fill=False)
