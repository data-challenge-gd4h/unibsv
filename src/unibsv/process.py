import datetime
import glob
import io
import logging
import math
import os
import re
import tempfile
from typing import Iterable

import cv2
import dateparser
import fitz
import nltk
import numpy as np
from PIL import Image as Image
from bigtree import findall, preorder_iter
from nltk import StemmerI, RegexpTagger
from nltk import edit_distance
from pandas import DataFrame
from unidecode import unidecode

from unibsv.configuration import get_config, CONFIG_REGIONS, CONFIG_STAGE_TITLES, CONFIG_STEMMER, CONFIG_PESTS, \
    CONFIG_LOCATION_HEADERS, CONFIG_LOCATIONS, CONFIG_STAGES, CONFIG_STAGE_LEVEL_MAPPING, CONFIG_STAGE_LEVELS, \
    StageLevels, CONFIG_STAGES_TABLE_GRAMMAR
from unibsv.preprocess import PdfContent, NODE_TYPE_LINE, NODE_TYPE_TABLE, NODE_TYPE_IMAGE

DATA_FIELD_REGION = "region"
DATA_FIELD_ZONE = "zone"
DATA_FIELD_DATE = "date"
DATA_FIELD_STAGE_MOST = "stage_most"
DATA_FIELD_STAGE_MINI = "stage_mini"
DATA_FIELD_STAGE_MAXI = "stage_maxi"
DATA_FIELD_PEST = "pest_name"
DATA_FIELD_RISK = "risk_level"
DATA_FIELDS = [DATA_FIELD_ZONE, DATA_FIELD_DATE, DATA_FIELD_PEST, DATA_FIELD_STAGE_MINI, DATA_FIELD_STAGE_MAXI,
               DATA_FIELD_RISK]
CSV_HEADERS = ["Fichier source", "Région", "Zone géographique", "Date",
               "Stade phénologique majoritaire", "Stade phénologique tardif", "Stade phénologique avancé",
               "Bioagresseur", "Niveau risque bioagresseur"]

#: label for risk evaluation
RISK_INTENSITY_LABEL = "risk_intensity"

#: label for pests
PEST_LABEL = "pest"

#: label for locations
LOCATION_LABEL = "location"


class Report:
    """
    The report to generate.
    """

    def __init__(self, pdf_trees: list[PdfContent] = None):
        self.pdf_trees = pdf_trees if pdf_trees else []
        self.stages = {}
        self.risks = {}

    def add_source(self, pdf_tree: PdfContent):
        self.pdf_trees.append(pdf_tree)

    def add_stage(self, source: str, region: str, zone: str, date: datetime.date,
                  stage_most: str, stage_mini: str, stage_maxi: str):
        """
        Add to the report a phenological stage analysis.
        :param source: the source file name
        :param region: the BSV region
        :param zone: the concerned region part, or the region itself for global analysis
        :param date: the BSV date
        :param stage_most: the most prevalent stage
        :param stage_mini: the least advanced stage
        :param stage_maxi: the most advanced stage
        """
        self.stages[(source, region, zone, date)] = (stage_most, stage_mini, stage_maxi)

    def add_risk(self, source: str, region: str, zone: str, date: datetime.date,
                 pest: str, risk_level: str = None):
        """
        Add to the report a risk analysis for a given pest.
        :param source: the source file name
        :param region: the BSV region
        :param zone: the concerned region part, or the region itself for global analysis
        :param date: the BSV date
        :param pest: the pest this is about
        :param risk_level: the evaluated risk for the given pest (None for secondary pests)
        """
        self.risks.setdefault((source, region, zone, date), [])
        self.risks[(source, region, zone, date)].append((pest, risk_level))


def ratio(s1: str, s2: str) -> float:
    """
    Gives Leveishtein ratio for 2 strings comparison.

    :param s1: a sequence
    :param s2: another sequence
    :return: levenshtein ratio, between [0,1]
    """
    ratio = 1 - edit_distance(s1, s2) / (len(s1) + len(s2))
    return ratio


def set_label_by_match(pdf: PdfContent, label: str, samples: Iterable[str], threshold: float = 1):
    """
    Match node text with samples list, if so,
    Set given label to the nodes and their descendents with the matching value of samples list.
    Arbitrary keep 6 first words and until ":" to separate title and paragraphs.

    :param pdf: a root PdfContent
    :param label: label to set on matching nodes
    :param samples: well-known expressions of a same category
    :param threshold: min ratio value, between 0 and 1, def. 1 (strict matching)
    """
    for n in findall(pdf, lambda n: hasattr(n, "text_clean")):
        text = n.text_clean

        # Extract 6 first words and until ":", excludes sentences more than 6 words not considered as title
        text = re.search(r"^([\w'-]+\s?){0,6}(?=$|:)", text)
        if not text:
            continue

        text = text.group().strip()
        if tmp := any_ratio_match(text, samples, threshold): n.labels[label] = tmp

    # Add label attribute to all matching descendents
    for n in preorder_iter(pdf, filter_condition=lambda n: n.labels.get(label)):
        for c in n.children:
            c.labels[label] = n.labels.get(label)


def process(pdf_tree: PdfContent, report: Report, config_file: str = None):
    """
    Process data to generate a report.

    :param pdf_tree: pre-processed source data
    :param report: the Report where processed data must be aggregated
    :param config_file: (optional) a configuration file to use, to override the defaults
    """
    config = get_config(config_file)
    report.add_source(pdf_tree)

    logging.info(f"Processing file [{pdf_tree.node_name}]...")

    bsv_region = resolve_region(pdf_tree, config)
    bsv_date = resolve_date(pdf_tree, config)
    logging.info(f"Resolved region {bsv_region} and date {bsv_date}")

    # phenological stages
    stages_per_location = resolve_stages(pdf_tree, config)
    for location, stages in stages_per_location.items():
        report.add_stage(source=pdf_tree.name, region=bsv_region, zone=location, date=bsv_date,
                         stage_most=stages[StageLevels.most.value],
                         stage_mini=stages[StageLevels.mini.value],
                         stage_maxi=stages[StageLevels.maxi.value])

    # pests & risks
    pests = resolve_pests(pdf_tree, bsv_region, config)
    for pest, locations in pests.items():
        for location, risk_level in locations.items():
            report.add_risk(source=pdf_tree.name, region=bsv_region, zone=location, date=bsv_date,
                            pest=pest, risk_level=risk_level)


def evaluate_risk_gauges(pdf: PdfContent):
    """
    Add labels on risk gauges.
    :param pdf: some PdfContent
    """
    tmp_dir = tempfile.mkdtemp()
    for i, image in enumerate(pdf.find_nodes(NODE_TYPE_IMAGE)):
        risk_intensity = process_image(image, i, tmp_dir)
        if risk_intensity:
            image.labels[RISK_INTENSITY_LABEL] = risk_intensity
    # delete temp files
    tmp_files = glob.glob(f"{tmp_dir}/*")
    for f in tmp_files:
        os.remove(f)
    os.rmdir(tmp_dir)


def resolve_pests(pdf: PdfContent, bsv_region: str, config: dict) -> dict:
    """
    Resolve pests with their locations and risk level.
    :param pdf: a pre-processed BSV
    :param bsv_region: a default location
    :param config: the config with known pests and locations
    :return: nested dicts of pest -> location -> risk level
    """
    # find & annotate all risk gauges
    evaluate_risk_gauges(pdf)

    # find & annotate pests & locations
    set_label_by_match(pdf, PEST_LABEL, config[CONFIG_PESTS], 0.75)
    set_label_by_match(pdf, LOCATION_LABEL, config[CONFIG_LOCATIONS], 0.85)

    # aggregate results
    pests = {}
    for pest_node in findall(pdf, lambda n: n.labels.get(PEST_LABEL)):
        pest = pest_node.labels.get(PEST_LABEL)
        pests.setdefault(pest, {})

        # try to resolve a precise location
        location = bsv_region
        location_nodes = [loc.labels.get(LOCATION_LABEL)
                          for loc in pest_node.ancestors
                          if loc.labels.get(LOCATION_LABEL)]
        if len(location_nodes) > 0:
            location = location_nodes[0]

        # try to resolve a risk level for this pest
        risk_level = pests[pest].get(location)
        if not risk_level:
            risk_level = resolve_risk_level(pest_node)
            pests[pest][location] = risk_level

    return pests


def resolve_risk_level(pest_node: PdfContent) -> str:
    """
    Search for the max/worst risk level among the given node's descendants, then in its entire section.
    :param pest_node: where a pest was found
    :return: a risk level, or None if not found
    """
    # 1°/ search for scales among images (among direct descendants, then cousins & nephews)
    for node in [pest_node, pest_node.parent]:
        pest = pest_node.labels.get(PEST_LABEL)
        all_risks = [
            (image.name, image.labels.get(RISK_INTENSITY_LABEL))
            for image in node.find_nodes(NODE_TYPE_IMAGE)
            if image.labels.get(RISK_INTENSITY_LABEL)
        ]
        if len(all_risks) > 0:
            image_name, max_risk = max(all_risks, key=lambda pair: pair[1])
            logging.info(f"Resolved risk for [{pest}] from image [{os.path.basename(image_name)}]: {max_risk:1.0f}%")
            return f"{max_risk:1.0f}%"

    # 2°/ search for a risk expression in plain text
    return None  # TODO


def write_report(report: Report, output_file_path):
    """
    Write the given report as a CSV file.

    :param report: the generated data to write
    :param output_file_path: the target file path on the file system
    """

    if len(report.stages) + len(report.risks) == 0:
        logging.warning("No data to write")
        with open(output_file_path, "w", encoding='utf-8-sig') as csv:
            csv.write(";".join(CSV_HEADERS) + "\n")  # just write the headers
        logging.info(f"Written empty output file {output_file_path}")
    else:
        row_keys = report.stages.keys() | report.risks.keys()
        for row_key in row_keys:  # fill missing tuples to enable stages-only or risks-only reports
            report.stages.setdefault(row_key, ("", "", ""))
            report.risks.setdefault(row_key, [("", "")])
        array = [
            row_key + report.stages[row_key] + risk
            for row_key in row_keys
            for risk in report.risks[row_key]
        ]
        array.sort()  # stable output
        data_frame = DataFrame(data=array, dtype=str)
        data_frame.to_csv(path_or_buf=output_file_path, header=CSV_HEADERS, index=False, sep=";", na_rep="", encoding='utf-8-sig')
        logging.info(f"Written {len(report.stages) + len(report.risks)} data lines into output file {output_file_path}")


def resolve_date(pdf: PdfContent, config: dict) -> datetime.date:
    """
    Resolve the date of the given BSV.
    :param pdf: a pre-processed BSV
    :param config: the config with known region names
    :return: a proper date, or a default one if none was found
    """
    for line in pdf.find_nodes(NODE_TYPE_LINE):
        date_matches = re.match(r".*\b(\d+\s+\w+\s+\d+|\d+\s*/\s*\d+\s*/\s*\d+)\b.*", line.text)
        if date_matches:
            for date_match in date_matches.groups():
                date = dateparser.parse(date_match, locales=["fr"])
                if date:
                    return date.date()
    return datetime.date(1900, 1, 1)


def resolve_region(pdf: PdfContent, config: dict) -> str:
    """
    Resolve the region of the given BSV.
    :param pdf: a pre-processed BSV
    :param config: the config with known region names
    :return: a known region name, or None if none was found
    """
    regions = config[CONFIG_REGIONS]
    for line in pdf.find_nodes(NODE_TYPE_LINE):
        for region in regions:
            if region in line.text:
                return region
    return None


def resolve_stages(pdf: PdfContent, config: dict) -> dict[str:tuple[str, str]]:
    """
    Resolve phenological stages for any location found in the BSV.
    Stages are searched for as most/min/max states (returned as a tuple), whenever possible.
    Tries to:
    1. locate a section with an appropriate title to work with;
    2. process tables first, then text lines, to find known expressions in there.
    :param pdf: a pre-processed BSV
    :param config: the config with known location names and stage-related words
    :return: a dict of most/min/max stages, indexed by location
    """
    title_sections = [
        line
        for line in pdf.find_nodes(NODE_TYPE_LINE)
        if any_match(line.text, config[CONFIG_STAGE_TITLES], config[CONFIG_STEMMER])
    ]
    if len(title_sections) == 0:
        return {}  # failed to locate section

    max_title_size = max([li.metadata["size"] for li in title_sections])
    title_section = [line for line in title_sections if line.metadata["size"] == max_title_size][0]

    # 1°/ try to find a summary table
    for node in title_section.find_nodes(NODE_TYPE_TABLE):
        stages = resolve_stages_from_table(node.table, config)
        if len(stages) > 0:
            return stages  # return asap, skipping other tables

    # 2°/ full-text search
    lines = [line.text for line in title_section.find_nodes(NODE_TYPE_LINE)]
    return resolve_stages_from_lines(lines, config)


def resolve_stages_from_lines(lines: list[str], config: dict):
    return {}  # TODO


def to_stems(text: str, stemmer: StemmerI) -> list[str]:
    """
    Split the given text into ascii stems.
    :param text: some text
    :param stemmer: an instance of stemmer
    :return: a list of stems
    """
    if not text:
        return []
    words = [word for word in text.split()]
    return [unidecode(stemmer.stem(word)) for word in words if not re.fullmatch(r"\W*", word)]


def any_match(text: str, samples: Iterable[str], stemmer: StemmerI) -> str:
    """
    Test if the given text contains any one of the given samples.
    Comparison is based on stems.
    :param text: some text
    :param samples: well-known expressions of a same category
    :param stemmer: an instance of stemmer
    :return: any sample found in the text, or None
    """
    for example in samples:
        if all(word in to_stems(text, stemmer) for word in to_stems(example, stemmer)):
            return example
    return None


def any_ratio_match(text: str, samples: Iterable[str], threshold: float = 1) -> str:
    """
    Test if the given text match any one of the given samples.
    Comparison is based on levenstein ratio.
    :param text: some text
    :param samples: well-known expressions of a same category
    :param threshold: min ratio value, between 0 and 1, def. 1 (strict matching)
    :return: any sample found in the text, or None
    """
    # Create list with each words
    words = text.split()
    words.append(text)

    # Search any matching
    for w in words:
        for a in samples:
            if ratio(w.casefold(), a.casefold()) >= threshold:
                return a
    return None


def resolve_stages_from_table(table: DataFrame, config: dict) -> dict[str:tuple[str, str, str]]:
    """
    Resolve phenological stages from the given table, if applicable.
    Stages can be resolved if the 1st line/column is about zones and following lines/columns describe stages.
    :param table: some table in a stage-related section
    :param config: the config with words about locations and stages
    :return: tuples of stage descriptions, indexed by zone in a dict; or an empty dict if the given table is irrelevant
    """
    stemmer = config[CONFIG_STEMMER]
    cell_types = table.applymap(
        lambda cell: [
            label
            for label in [CONFIG_LOCATION_HEADERS, CONFIG_LOCATIONS, CONFIG_STAGES, CONFIG_STAGE_LEVELS]
            if any_match(cell, config[label], stemmer)
        ]
    )

    # table orientation (when in doubt, we consider locations are in the 1st column)
    locations_in_1st_line = all(
        any([CONFIG_LOCATIONS in cell, CONFIG_LOCATION_HEADERS in cell])
        for cell in cell_types.values[0]
    )
    if locations_in_1st_line:  # transpose to have locations in the 1st column
        table = table.transpose()
        cell_types = cell_types.transpose()

    # try to get hints from headers (when applicable)
    first_header_is_for_locations = CONFIG_LOCATION_HEADERS in cell_types.values[0][0]
    other_headers_are_for_stages = all(CONFIG_STAGE_LEVELS in cell for cell in cell_types.values[0][1:])
    stage_headers = [
        config[CONFIG_STAGE_LEVEL_MAPPING].get(
            any_match(header, config[CONFIG_STAGE_LEVEL_MAPPING].keys(), config[CONFIG_STEMMER])
        ).value
        for header in table.values[0][1:]
    ] if first_header_is_for_locations and other_headers_are_for_stages else None

    # index raw stage data by location
    raw_stages: dict[str:list[str]] = {
        table.values[i][0]: [
            table.values[i][j]  # if CONFIG_STAGES in cell_types.values[i][j] else None
            for j in range(1, len(table.values[i]))
        ]
        for i in range(1 if stage_headers else 0, len(table.values))
    }

    if len([v for v in raw_stages if v]) == 0:
        return {}  # found nothing, skip table

    # now trying to infer most/mini/maxi stage tuples from raw data
    stages = {}
    for zone, raw_stage in raw_stages.items():
        if stage_headers:  # using headers
            stage_values = [None, None, None]
            for i in range(len(stage_headers)):
                stage_values[stage_headers[i]] = raw_stage[i]
            if not stage_values[1] and not stage_values[2]:  # fill empty min/max stages
                stage_values[1] = stage_values[0]
                stage_values[2] = stage_values[0]
            stages[zone] = tuple(stage_values)
        else:  # no help from headers
            try:
                stages[zone] = infer_stages_from(raw_stage, config)
            except Exception as e:
                logging.error(f"Could not parse table content: {raw_stage}")
                default_text = " ".join([t for t in raw_stage if t])
                stages[zone] = (default_text, default_text, default_text)

    return stages


def infer_stages_from(source_text: list[str], config: dict) -> tuple:
    """
    Try to extract most/min/max phenological stages from the given text.
    :param source_text: some text about stages
    :param config: the configuration with stage-related words
    :return: a tuple of most/min/max stages, in this order, defaulting to the whole text when failing
    """
    split_text = " ".join([
        t.replace("-", " - ").replace("/", " / ")  # these separators have meaning here, read them as words
        for t in source_text if t
    ]).split()

    stemmer = config[CONFIG_STEMMER]
    clean_source = [
        # stem words, but preserve letter-based stages & min/max separator
        to_stems(w, stemmer)[0] if not re.fullmatch(r'^[àA-Z/-]$', w.strip()) else w.strip()
        for w in split_text
    ]

    # Work on tags to simplify grammar
    tags = RegexpTagger([
        (r'^[A-Z]$', 'Stage_Value'),  # letter-based taxonomy
        (r'^\d+[%]?$', 'Stage_Value'),  # numeric values
        (r'majoritair', 'Most'),
        (r'^[/-]$', 'Min_To_Max1'),  # value separators
        (r'^à$', 'Min_To_Max2'),  # stage separators
        (r'.*', 'Stage_Type'),  # default tag
    ]).tag(clean_source)
    tags_grammar = nltk.CFG.fromstring(config[CONFIG_STAGES_TABLE_GRAMMAR])
    tags_parser = nltk.ChartParser(tags_grammar)
    tags_sentence = [t[1] for t in tags]
    parsed_trees = list(tags_parser.parse(tags_sentence))

    # use the entire text as default value
    default_text = " ".join(split_text)
    result = [default_text, default_text, default_text]

    if len(parsed_trees) == 0:
        return tuple(result)  # grammar cannot generate the given text, just return defaults

    parsed_tree = parsed_trees[0]  # TODO choose more wisely if 2+ trees

    # 1°/ search for any "most"-type stage
    words_before = 0
    se_node = parsed_tree
    se_leaves = []
    for i in range(len(parsed_tree)):
        node = parsed_tree[i]
        node_length = len(node.leaves())
        if node.label() == "SEMost":
            most_leaves = split_text[words_before:words_before + node_length]
            result[StageLevels.most.value] = " ".join(most_leaves).replace(" - ", "-").replace(" / ", "/")
        elif node.label() == "SE":
            se_node = node  # remaining content
            se_leaves = split_text[words_before:words_before + node_length]
        words_before += node_length

    # 2°/ search for min-to-max separator in the remaining content
    min_leaves = se_leaves
    max_leaves = se_leaves
    if "MTM2" in [se_node[i].label() for i in range(len(se_node))]:  # SVE MTM2 SVE
        node_lengths = [len(se_node[i].leaves()) for i in range(len(se_node))]
        min_leaves = se_leaves[0:node_lengths[0]]
        max_leaves = se_leaves[node_lengths[0] + node_lengths[1]:]
        # TODO complete the "min"/"max" stage with right/left-hand ST members when applicable, eg. "3 à 4 mm" => "3 mm"
    # TODO (?) try to use MTM1 as a fallback to resolve an interval, eg. "baies 3-4 mm" => "baies 3 mm"/"baies 4 mm"
    result[StageLevels.mini.value] = " ".join(min_leaves).replace(" - ", "-").replace(" / ", "/")
    result[StageLevels.maxi.value] = " ".join(max_leaves).replace(" - ", "-").replace(" / ", "/")

    return tuple(result)


def calculate_color_diversity(image):
    """
    Compute the color entropy.
    :param image: the image
    :return: real value that gives an estimate of the image color entropy
    """
    image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    # Calculate the color histogram
    histogram = cv2.calcHist([image_rgb], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])

    # Normalize the histogram
    histogram /= histogram.sum()

    # Calculate the color entropy
    entropy = -np.sum(histogram * np.log2(histogram + np.finfo(float).eps))

    return entropy

    """
    Get PIL image from encoding.
    :param img_node: an image node
    :return: a PIL image
    """


def calculate_angle(pt1, pt2):
    """
    Calculate angle slope of a line from its 2 points.
    :param pt1: 1st line point
    :param pt2: 2nd line point
    :return: the line angle slope
    """
    dx = pt2[0] - pt1[0]
    dy = pt2[1] - pt1[1]
    angle = math.atan2(dy, dx) * 180 / math.pi
    return angle


def increase_line_length(line_start, line_end, increase_length):
    """
    Increase the length of a line from its 2 points (for visualization purpose).
    :param line_start: line first point
    :param line_end: line 2nd point
    :param increase_length: desired extension
    :return: the 2 points of the extended line
    """

    # Calculate the vector from start to end
    dx = line_end[0] - line_start[0]
    dy = line_end[1] - line_start[1]

    # Normalize the vector
    length = (dx ** 2 + dy ** 2) ** 0.5
    dx_normalized = dx / length
    dy_normalized = dy / length

    line_start_extended = (int(line_start[0] - increase_length * dx_normalized),
                           int(line_start[1] - increase_length * dy_normalized))
    line_end_extended = (int(line_end[0] + increase_length * dx_normalized),
                         int(line_end[1] + increase_length * dy_normalized))

    return line_start_extended, line_end_extended


def find_intersection(line1_start, line1_end, line2_start, line2_end):
    """
    Compute the intersection between 2 lines.
    :param line1_start: 1st line 1st point
    :param line1_end: 1st line 2nd point
    :param line1_start: 2nd line 1st point
    :param line1_end: 2nd line 2nd point
    :return: the intersection point
    """
    # Convert points to homogeneous coordinates (3D)
    line1_start_homogeneous = np.array([line1_start[0], line1_start[1], 1])
    line1_end_homogeneous = np.array([line1_end[0], line1_end[1], 1])
    line2_start_homogeneous = np.array([line2_start[0], line2_start[1], 1])
    line2_end_homogeneous = np.array([line2_end[0], line2_end[1], 1])

    # Calculate the cross product of the lines
    cross_product_1 = np.cross(line1_start_homogeneous, line1_end_homogeneous)
    cross_product_2 = np.cross(line2_start_homogeneous, line2_end_homogeneous)

    # Calculate the intersection point
    intersection_homogeneous = np.cross(cross_product_1, cross_product_2)
    intersection_point = (int(intersection_homogeneous[0] / intersection_homogeneous[2]),
                          int(intersection_homogeneous[1] / intersection_homogeneous[2]))

    return intersection_point


def compute_risk_intensity(path, original_img):
    """
    Compute the risk intensity of the image containing only the black arrow thanks
    to Canny edge detection and hough lines. Draw visualization and save them on the original image.
    :param path: path of the image containing only the black arrow
    :param original_img: original image
    :return: the hough image the orignal image annotated, and the risk intensity
    """
    img = cv2.imread(path)

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(gray, 50, 150, apertureSize=3)

    lines = cv2.HoughLinesP(edges, 1, np.pi / 180, 10, minLineLength=1, maxLineGap=10)

    oriented_lines = []

    for line in lines:
        x1, y1, x2, y2 = line[0]

        angle = calculate_angle((x1, y1), (x2, y2))
        lv = np.array([x1, y1]) - np.array([x2, y2])
        line_norm = np.linalg.norm(lv)

        if 30 < np.abs(angle) < 80:
            cv2.line(img, (x1, y1), (x2, y2), (0, 0, 255), 2)
            cv2.line(original_img, (x1, y1), (x2, y2), (0, 0, 255), 2)

            oriented_lines.append(((x1, y1, x2, y2), line_norm, angle))

    oriented_lines = sorted(oriented_lines, key=lambda x: x[1], reverse=True)

    to_plot = [oriented_lines[0]]
    for oriented_line in oriented_lines:
        angle = oriented_line[2]
        if angle * oriented_lines[0][2] < 0:
            to_plot.append(oriented_line)
            break

    intersecting = []
    for oriented_line in to_plot:
        norm = oriented_line[1]
        angle = oriented_line[2]
        oriented_line = oriented_line[0]

        t = increase_line_length((oriented_line[0], oriented_line[1]), (oriented_line[2], oriented_line[3]), 50)

        intersecting.append(t)

        cv2.line(img, (oriented_line[0], oriented_line[1]), (oriented_line[2], oriented_line[3]), (0, 255, 0), 50)
        cv2.line(original_img, (oriented_line[0], oriented_line[1]), (oriented_line[2], oriented_line[3]), (0, 255, 0), 50)

        text_position = (oriented_line[0] + 10, oriented_line[1] - 10)

        cv2.putText(img, f"{angle}_{norm}", text_position, cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
        cv2.putText(original_img, f"{angle}_{norm}", text_position, cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

    l1 = intersecting[0]
    l2 = intersecting[1]

    intersection_point = find_intersection(l1[0], l1[1], l2[0], l2[1])

    cv2.circle(img, intersection_point, 5, (255, 255, 0), 100)
    cv2.circle(original_img, intersection_point, 5, (255, 255, 0), 100)

    scale_cursor_start = (intersection_point[0], 0)
    scale_cursor_end = (intersection_point[0], img.shape[0])

    cv2.circle(original_img, scale_cursor_start, 5, (255, 255, 0), 100)
    cv2.circle(original_img, scale_cursor_end, 5, (255, 255, 0), 100)
    cv2.line(original_img, scale_cursor_start, scale_cursor_end, (0, 0, 255), 2)

    risk_intensity = (intersection_point[0] / original_img.shape[1]) * 100

    cv2.putText(original_img, f"{int(risk_intensity)}%",
                (intersection_point[0] + 20, intersection_point[1] + 20),
                cv2.FONT_HERSHEY_SIMPLEX, 25, (0, 0, 255), 30)

    return img, original_img, risk_intensity


def is_risk_scale(col_entropy, width_height_ratio) -> bool:
    """
    Assess whether an image is a scale-based risk given the color entropy and the dimensions ratio.
    :param col_entropy: path of the image containing only the black arrow
    :param width_height_ratio: image dimensions ratio
    :return: a boolean that says whether it is a scale-based risk
    """
    return col_entropy > 3 and width_height_ratio > 6


def process_image(image_node, i, output_folder=None):
    """
    Process an image node. If this image is a scale-based risk, it will return the estimated risk intensity.
    :param image_node: image node
    :param i: image index
    :param output_folder: path of the folder to save the intermediate image processing steps
    :return: the risk intensity associated to the image
    """
    if not output_folder:
        output_folder = tempfile.mkdtemp()
        logging.debug(f"working in temp directory {output_folder}")
    elif not os.path.exists(output_folder):
        os.makedirs(output_folder)

    imgp = Image.open(io.BytesIO(image_node.image)).convert('RGB')
    img = np.array(imgp)

    img_ratio = img.shape[1] / img.shape[0]
    col_entropy = calculate_color_diversity(img)

    if is_risk_scale(col_entropy, img_ratio):
        bbox = image_node.metadata["bbox"]
        offset = 25
        region_coords = (bbox[0], bbox[1] - offset, bbox[2], bbox[3] + offset)

        # Render the region as a pixmap
        with fitz.open(image_node.root.source_file) as doc:
            page = doc[image_node.metadata["page"]]
            pix = page.get_pixmap(matrix=(fitz.Matrix(50, 50)), clip=region_coords)

        # Convert the pixmap to a PIL image
        image = Image.frombytes("RGB", [pix.width, pix.height], pix.samples)

        imgp.save(f"{output_folder}/{i}_{int(col_entropy)}_{int(img_ratio)}.png")
        image.save(f"{output_folder}/{i}_{int(col_entropy)}_{int(img_ratio)}_arrow.jpg", 'JPEG')

        img_arrow = cv2.imread(f"{output_folder}/{i}_{int(col_entropy)}_{int(img_ratio)}_arrow.jpg")

        grayscale = cv2.cvtColor(img_arrow, cv2.COLOR_BGR2GRAY)
        # Apply thresholding to create a binary image
        _, thresholded = cv2.threshold(grayscale, 1, 255, cv2.THRESH_BINARY)

        # Convert the binary image to BGR color image
        result = cv2.cvtColor(thresholded, cv2.COLOR_GRAY2BGR)
        cv2.imwrite(f"{output_folder}/{i}_{int(col_entropy)}_{int(img_ratio)}_only_arrow.png", result)

        ihough, annotated_original, risk_intensity = compute_risk_intensity(
            f"{output_folder}/{i}_{int(col_entropy)}_{int(img_ratio)}_only_arrow.png", img_arrow)

        cv2.imwrite(f"{output_folder}/{i}_{int(col_entropy)}_{int(img_ratio)}_only_hough.png", ihough)

        cv2.imwrite(f"{output_folder}/{i}_{int(col_entropy)}_{int(img_ratio)}_annotated.png", annotated_original)

        return risk_intensity
