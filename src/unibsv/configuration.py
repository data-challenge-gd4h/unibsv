import logging
from enum import Enum

from nltk.stem.snowball import FrenchStemmer
from ruamel import yaml

CONFIG_REGIONS = "regions"
CONFIG_LOCATION_HEADERS = "location_headers"
CONFIG_LOCATIONS = "locations"
CONFIG_STAGE_TITLES = "stage_titles"
CONFIG_STAGE_LEVELS = "stage_levels"
CONFIG_STAGE_LEVEL_MAPPING = "stage_levels_map"
CONFIG_STAGES = "stages"
CONFIG_STAGE_VALUES = "stage_values"
CONFIG_STAGES_TABLE_GRAMMAR = "stage_tables_grammar"
CONFIG_PESTS = "pests"
CONFIG_STEMMER = "stemmer"


class StageLevels(Enum):
    """
    Stage levels. Int values match with their index in reported tuples.
    """
    most = 0
    mini = 1
    maxi = 2


#: default configuration values
defaults = {
    CONFIG_REGIONS: [
        "Alsace",
        "Auvergne-Rhône-Alpes",
        "Bourgogne-Franche-Comté",
        "Champagne",
        "Languedoc-Roussillon",
        "Nouvelle-Aquitaine",
        "PACA",
    ],
    CONFIG_LOCATION_HEADERS: [
        "Territoire",
        "Vignoble",
        "Département",
        "Cépage",
    ],
    CONFIG_LOCATIONS: [
        "Secteur",
        "Nord Aquitaine",
        "Sud Drôme",
        "Côtes du Rhône",
        "Vallée du Rhône",
        "Sud Luberon",
        "Bouches du Rhône",
        "Ste Victoire",
        "Provence",
        "Saône et Loire",
        "Côte d'Or",
        "Yonne",
        "Nièvre",
        "Franche Comté",
        "Aude",
        "Gard",
        "Hérault",
        "Pyrénées-Orientales",
        "Beaujolais",
        "Coteaux du Lyonnais",
        "Forez",
        "Roannais",
        "Côtes du Rhône nord",
        "Diois",
        "Savoie",
        "Bugey",
        "Balmes dauphinoises",
        "Sud Ardèche",
    ],
    CONFIG_STAGE_TITLES: [
        "stades végétatifs",
        "stades phénologiques",
        "phénologie",
    ],
    CONFIG_STAGE_LEVEL_MAPPING: {
        "majoritaire": StageLevels.most,
        "mini": StageLevels.mini,
        "tardif": StageLevels.mini,
        "maxi": StageLevels.maxi,
        "avancé": StageLevels.maxi,
    },
    CONFIG_STAGES: [
        "stade",
        "baies",
        "fleur",
        "fermeture",
        "floraison",
        "nouaison",
        "grain",
        "récolte",
        "véraison",
    ],
    CONFIG_STAGES_TABLE_GRAMMAR: """
    ## Grammar on tags rather than actual words, to simplify things
    
    # Overall content may, or may not, have a "Most" expression
    S -> SE | SEMost SE | SE SEMost
    
    # "Most" stage expression
    SEMost -> SE M | M SE
    
    # Either a single stage expression, or a "Min-to-Max" expression
    SE -> SCE | SCE MTM2 SCE
    
    # A stage composite expression, with mixed & optional types and values
    SCE -> SV | ST | SCE ST | ST SCE | SCE SV | SV SCE
    
    # Words (tags)
    ST -> "Stage_Type"
    SV -> "Stage_Value" | SV MTM1 SV
    MTM1 -> "Min_To_Max1"
    MTM2 -> "Min_To_Max2"
    M -> "Most"
""",
    CONFIG_PESTS: [
        "Mildiou",
        "Oïdium",
        "Black-rot",
        "Blackrot",
        "Botrytis",
        "Pourriture grise",
        "Maladie du bois",
        "Excoriose",
        "Eutypiose",
        "Cicadelles FD",
        "Vers de la grappe",
        "Tordeuses",
        "Tordeuses de la grappe",
        "Eudémis",
        "Cochylis",
        "Pyrale",
        "Pyrale de la vigne",
        "Pyrale du Daphné",
        "Ephestia sp.",
        "Eulia",
        "Ephestia unicolorella",
        "Noctuelles",
        "Boarmies",
        "Mange bourgeons",
        "Mange-bourgeons",
        "Acariose",
        "Acariens",
        "Erinose",
        "Araignées rouges",
        "Cicadelle verte",
        "Cicadelle vectrice de la flavescenc",
        "Cicadelle de la flavescence dorée",
        "Flavescence dorée",
        "Cochenilles",
        "Escargots",
        "Nécrose bactérienne",
        "Thrips",
        "Cigarier",
        "Ephippigère",
        "Malacome du Portugal",
        "Cochenille floconneuse",
        "Metcalfa pruinosa",
        "Altise",
        "Petit hanneton de la vigne",
        "Punaise verte",
        "Drosophile",
        "Drosophila Suzukii",
    ],
}
defaults[CONFIG_STAGE_LEVELS] = defaults[CONFIG_STAGE_LEVEL_MAPPING].keys()


def get_config(config_file: str = None) -> dict:
    """
    Get the configuration.
    Optionally, defaults can be overridden by a given YAML file.
    :param config_file: (optional) a local file with YAML config
    :return: the default configuration, optionally overridden
    """
    config = defaults.copy()
    if config_file is not None:
        with open(config_file, 'r') as stream:
            try:
                logging.info(f"Overriding configuration using {config_file}")
                config |= yaml.safe_load(stream)
            except yaml.YAMLError as e:
                logging.error(f"Could not load YAML config: {config_file}", e)
    config[CONFIG_STEMMER] = FrenchStemmer()
    return config
