import logging
import operator
import os
import re
import tempfile
from itertools import chain
from os.path import basename
from typing import Iterable, Sequence

import pandas.io.pytables
from ascii_magic import AsciiArt
from bigtree import shift_nodes, Node, yield_tree, findall
from fitz import fitz, Rect, Point
from nltk import edit_distance
from pandas import DataFrame
from typing_extensions import Self

# formatting-related constants for pretty-printing
SHORT_FORMAT_SPEC = "short"
FULL_FORMAT_SPEC = "full"
FILTERED_METADATA_KEYS = ["font", "size", "width", "height", "format", "color"]

#: label to classify nodes
NODE_TYPE_LABEL = "node_type"
NODE_TYPE_DOCUMENT = "DOCUMENT"
NODE_TYPE_PAGE = "PAGE"
NODE_TYPE_LINE = "LINE"
NODE_TYPE_SPAN = "SPAN"
NODE_TYPE_IMAGE = "IMAGE"
NODE_TYPE_TABLE = "TABLE"

#: max footer outliers (1st & last pages may have their own layout)
FOOTER_MAX_OUTLIERS = 2

#: max edit distance between footer lines (typically the page number)
FOOTER_DISTANCE_THRESHOLD = 2

#: The max width of lines in tables
TABLE_LINE_WIDTH = 3


class PdfContent(Node):
    """
    Holds content extracted from a PDF file as a tree structure.
    """

    def __init__(self, name: str,
                 text: str = None,
                 image: bytearray = None,
                 table: DataFrame = None,
                 metadata: dict = None,
                 labels: dict = None,
                 parent: Self = None):
        """
        Builds a new PdfContent based on either text or an image.

        :param name: (inherited) a unique label for the content node
        :param parent: (inherited) a parent PdfContent, if not the whole PDF file itself (i.e. the tree's root)
        :param text: some text read from the PDF
        :param image: an image extracted from the PDF
        :param metadata: a dict of misc metadata about the text/image source
        :param labels: a dict of metadata about the text/image with semantic value for processing (e.g. classifiers)
        """

        super().__init__(name, parent=parent, text=text, image=image, table=table, labels=labels, metadata=metadata)

    def is_typed(self, node_type: str) -> bool:
        return self.labels[NODE_TYPE_LABEL] == node_type

    def __format__(self, format_spec) -> str:
        """
        Print format of this PdfContent.
        The "full" format_spec displays all metadata. The "short" one displays no metadata at all.

        :return: a printable representation of the PDF
        """

        def printable_text(n: PdfContent) -> str:
            if n.is_typed(NODE_TYPE_LINE) or n.is_typed(NODE_TYPE_TABLE):
                return n.text
            if n.is_typed(NODE_TYPE_IMAGE) and format_spec == FULL_FORMAT_SPEC:
                return to_ascii(n.image)
            if format_spec != SHORT_FORMAT_SPEC:
                return n.text
            return ''

        def printable_metadata(n: PdfContent) -> str:
            if n.metadata is None or format_spec == SHORT_FORMAT_SPEC:
                return ''
            if format_spec == FULL_FORMAT_SPEC:
                return "{} {}".format(n.metadata, n.labels)
            return "{} {}".format({k: v for k, v in n.metadata.items() if k in FILTERED_METADATA_KEYS}, n.labels)

        return "\n".join([
            "{}{}[{}] {} {}".format(branch, stem, node.node_name, printable_text(node), printable_metadata(node))
            for branch, stem, node in yield_tree(self, style="const")
            if format_spec != SHORT_FORMAT_SPEC or not node.is_typed(NODE_TYPE_SPAN)
        ])

    def find_nodes(self, *node_types) -> list[Self]:
        """
        Returns all sub-nodes of the given type(s).
        :return: a tuple of PdfContents
        """
        return list(findall(self, condition=lambda node: node.labels[NODE_TYPE_LABEL] in node_types))


def read_pdf(file_path) -> PdfContent:
    """
    Reads a source PDF file as a PdfContent.
    The returned data structure is pre-processed: its content is as clean as possible to ease actual processing.

    :param file_path: source PDF path on the file system
    """

    with fitz.open(file_path) as doc:
        logging.info(f"Reading file [{file_path}]...")
        root_node = PdfContent(name=basename(file_path),
                               labels={NODE_TYPE_LABEL: NODE_TYPE_DOCUMENT},
                               metadata=doc.metadata)
        root_node.source_file = file_path
        for page in doc:
            blocks_list = page.get_text("dict", sort=True)["blocks"]
            page_node = PdfContent(name=f"page-{page.number}",
                                   parent=root_node,
                                   metadata={"page_idx": page.number, "tables": detect_tables(page)},
                                   labels={NODE_TYPE_LABEL: NODE_TYPE_PAGE},
                                   text=f"{len(blocks_list)} blocks")
            for block_idx, block in enumerate(blocks_list):
                if "image" in block:
                    image = bytearray(block["image"])
                    image_metadata = {k: v for k, v in block.items() if k != "image"} | {"block_idx": block_idx}
                    image_metadata["page"] = page.number
                    PdfContent(name=f"img-{page.number}.{block_idx}",
                               parent=page_node,
                               labels={NODE_TYPE_LABEL: NODE_TYPE_IMAGE},
                               image=image,
                               metadata=image_metadata)
                if "lines" in block:
                    for line_idx, line in enumerate(block["lines"]):
                        spans = [span["text"] for span in line["spans"]]
                        line_metadata = {k: v for k, v in line.items() if k != "spans"} | {"block_idx": block_idx}
                        line_node = PdfContent(name=f"line-{page.number}.{block_idx}.{line_idx}",
                                               parent=page_node,
                                               labels={NODE_TYPE_LABEL: NODE_TYPE_LINE},
                                               text=''.join(spans),
                                               metadata=line_metadata)
                        for span_idx, span in enumerate(line["spans"]):
                            PdfContent(name=f"span-{page.number}.{block_idx}.{line_idx}.{span_idx}",
                                       parent=line_node,
                                       labels={NODE_TYPE_LABEL: NODE_TYPE_SPAN},
                                       text=span["text"],
                                       metadata={k: v for k, v in span.items() if k != "text"})

    sanitize(root_node)
    return root_node


def sanitize(pdf: PdfContent):
    """
    Process in place the given PdfContent to ensure it's in a convenient format for further processing.
    * Based on font/size metadata, aggregate spans and lines into paragraphs
    * Include tables whenever possible
    * Remove useless lines whenever possible (e.g. footers)
    * Infer a title/section hierarchy based on font size and line order
    * Simplify the tree structure (single page and no spans)
    * Clean text nodes
    :param pdf: a raw PdfContent
    """

    prune_empty_lines(pdf)
    infer_line_format(pdf)
    prune_spans(pdf)  # at this point spans are not useful anymore
    prune_footers(pdf)
    substitute_tables(pdf)
    merge_all_pages(pdf)
    merge_similar_sibling_lines(pdf)
    infer_titles(pdf)
    clean_text(pdf)


def prune_empty_lines(pdf: PdfContent):
    """
    Modify the given PdfContent in place, removing all empty lines.
    :param pdf: a root PdfContent
    """
    count = 0
    for line in pdf.find_nodes(NODE_TYPE_LINE):
        if re.fullmatch(" *", line.text):
            shift_nodes(pdf, [line.path_name], [None])
            count += 1
    logging.debug(f"removed {count} empty lines")


def prune_spans(pdf: PdfContent):
    """
    Modify the given PdfContent in place, removing all spans.
    :param pdf: a root PdfContent
    """
    for span in pdf.find_nodes(NODE_TYPE_SPAN):
        shift_nodes(pdf, [span.path_name], [None])


def prune_footers(pdf: PdfContent):
    """
    Search for footers in the given PDF and remove them in place.
    Assume each page may finish with a same footer block, differing from at most a page number.
    :param pdf: a multi-pages PDF document
    """
    footers: list[tuple[str, list[PdfContent]]] = []  # tuples with concatenated text content & nodes
    for page in pdf.find_nodes(NODE_TYPE_PAGE):
        last_block_idx = max([line.metadata["block_idx"] for line in page.find_nodes(NODE_TYPE_LINE)])
        last_lines = [line for line in page.find_nodes(NODE_TYPE_LINE) if line.metadata["block_idx"] == last_block_idx]
        footers.append(("\t".join([line.text for line in last_lines]), last_lines))

    for footer in footers:
        # compare with other text contents
        diff_list = [edit_distance(footer[0], f[0]) for f in footers if f[1] != footer[1]]
        # ignore outliers
        max_outliers = min(len(footers) - 1, FOOTER_MAX_OUTLIERS)  # keep 1+ elements
        for i in range(max_outliers):
            diff_list.remove(max(diff_list))
        # remove nodes with same content as others
        if len(diff_list) > 0 and max(diff_list) <= FOOTER_DISTANCE_THRESHOLD:
            logging.debug(f"removed footer at {[f.path_name for f in footer[1]]}: {footer[0]}")
            shift_nodes(pdf, [f.path_name for f in footer[1]], [None] * len(footer[1]))


def substitute_tables(pdf: PdfContent):
    """
    Replace raw lines in place by proper tables in the given PDF, when applicable.
    :param pdf: a PDF document
    """
    with fitz.open(pdf.source_file) as doc:
        for page in pdf.find_nodes(NODE_TYPE_PAGE):
            for table_idx, table in enumerate(page.metadata.get("tables", [])):
                content_lines = [[""] * len(tl) for tl in table]
                lines_with_content: dict[str, PdfContent] = {}  # ensure uniqueness using path as key
                first_content_node: PdfContent = None  # where the table begins
                for i, table_line in enumerate(table):
                    for j, table_cell in enumerate(table_line):
                        contained_lines = []
                        cell_rect = Rect(  # reduced area to avoid deleting lines just touching the table
                            table_cell.tl.x + TABLE_LINE_WIDTH,
                            table_cell.tl.y + TABLE_LINE_WIDTH,
                            table_cell.br.x - TABLE_LINE_WIDTH,
                            table_cell.br.y - TABLE_LINE_WIDTH,
                        )
                        for line in page.find_nodes(NODE_TYPE_LINE):
                            ri = Rect(cell_rect).intersect(Rect(line.metadata["bbox"]))
                            if not ri.is_empty:  # a line is included when its bbox touches the cell
                                contained_lines.append(line)
                        contained_lines.sort(key=lambda li: [(p.y, p.x) for p in [Rect(li.metadata["bbox"]).tl]][0])
                        for cl in contained_lines:
                            lines_with_content[cl.path_name] = cl
                            if first_content_node is None:
                                first_content_node = cl
                        if len(contained_lines) > 0:
                            # fetching text using source doc to handle lines spanning many cells
                            cell_text = doc.get_page_text(pno=page.metadata["page_idx"], clip=cell_rect).strip()
                            content_lines[i][j] = " ".join(cell_text.split())
                if len(lines_with_content) == 0:
                    continue  # skip empty tables

                # clean table content in this corner case: many cells are in fact margins and must be discarded
                purged_lines: list[list[str]] = [
                    [cell for cell in line if not re.fullmatch(" *", cell)]
                    for line in content_lines
                ]
                if [len(line) for line in purged_lines] == [len(purged_lines[0])] * len(purged_lines):
                    content_lines = purged_lines

                # build a table node
                content = pandas.DataFrame([tuple(cl) for cl in content_lines])
                metadata = {
                    "sources": sorted(lines_with_content.keys()),
                    "size": most_used_metadata(lines_with_content.values(), lambda n: n.metadata["size"]),
                    "font": most_used_metadata(lines_with_content.values(), lambda n: n.metadata["font"]),
                }
                table_node = PdfContent(name=f"table-{page.metadata.get('page_idx', 0)}.{table_idx}",
                                        parent=page,
                                        text="\n" + content.to_string(index=False, header=False, show_dimensions=True),
                                        table=content,
                                        metadata=metadata,
                                        labels={NODE_TYPE_LABEL: NODE_TYPE_TABLE})
                # replace line nodes
                move_node_before(pdf, table_node, first_content_node)
                for cl in lines_with_content.values():
                    shift_nodes(pdf, [cl.path_name], [None])


def move_node_before(pdf: PdfContent, node: PdfContent, to_node: PdfContent):
    """
    Modify the given tree in place to move a node before another one.
    :param pdf: a root node
    :param node: the node to move
    :param to_node: the node before which to move
    """
    parent = to_node.parent
    to_node_child_idx = parent.children.index(to_node)
    tmp_node = PdfContent(name="tmp", parent=pdf)  # required to reorder nodes
    shift_nodes(pdf, [node.path_name], [f"{tmp_node.path_name}/{node.node_name}"])
    right_siblings = [child for idx, child in enumerate(parent.children) if idx >= to_node_child_idx]
    shift_nodes(pdf,
                [n.path_name for n in right_siblings],
                [f"{tmp_node.path_name}/{n.node_name}" for n in right_siblings])
    shift_nodes(pdf,
                [n.path_name for n in tmp_node.children],
                [f"{parent.path_name}/{n.node_name}" for n in [node] + right_siblings])
    shift_nodes(pdf, [tmp_node.path_name], [None])


def regroup_equivalent_items(items: Sequence, eq) -> list[list]:
    """
    Regroup items based on an equivalence function.
    :param items: a few items
    :param eq: an equivalence function for items
    :return: a list of item groups with equivalent items
    """
    # identify item groups using tags
    tags = [i for i in range(len(items))]  # each item its own at first
    for i in range(len(items)):
        for j in range(i + 1, len(items)):
            if eq(items[i], items[j]):
                current_tag = tags[i]  # propagate j-th tag to all equivalent items (with i-th tag)
                for k in range(0, j + 1):
                    if tags[k] == current_tag:
                        tags[k] = tags[j]
    # regroup items by tag
    equivalence_classes = [
        [items[idx] for idx in range(len(tags)) if tags[idx] == i]
        for i in range(len(items))
    ]
    return [ec for ec in equivalence_classes if len(ec) > 0]  # ignore empty sets


def detect_tables(page: fitz.Page) -> list[list[list[Rect]]]:
    """
    Search for tables in the given page, based on grid-shaped sets of contiguous drawings.
    Note: tables can be made of either rects or thin lines. Here, rects are split into edges to handle lines only.
    :param page: a raw PDF page
    :return: a list of tables (as lists of Rect lists)
    """
    drawings = page.get_drawings()
    if drawings is None or len(drawings) < 4:
        return []  # no tables in this page

    # search for horizontal & vertical lines
    horizontal_lines: list[tuple[Point, Point]] = []  # (left, right)
    vertical_lines: list[tuple[Point, Point]] = []  # (top, bottom)
    for drawing in drawings:
        rect: Rect = drawing["rect"]
        if rect.height > TABLE_LINE_WIDTH:
            vertical_lines.append((rect.top_left, rect.bottom_left))
        if rect.width > TABLE_LINE_WIDTH:
            horizontal_lines.append((rect.top_left, rect.top_right))
        if rect.width > TABLE_LINE_WIDTH and rect.height > TABLE_LINE_WIDTH:
            horizontal_lines.append((rect.bottom_left, rect.bottom_right))
            vertical_lines.append((rect.top_right, rect.bottom_right))
    # merge overlaps to simplify the topology of stacked/redundant drawings
    merged_vertical_lines = merge_line_overlaps(vertical_lines, False)
    merged_horizontal_lines = merge_line_overlaps(horizontal_lines, True)

    # build possible cells
    cells: list[Rect] = []
    for left_edge in merged_vertical_lines[0]:  # "short" vertical lines
        right_edges = [line
                       for line in merged_vertical_lines[0]  # "short" vertical lines
                       if line[0].x > left_edge[0].x + TABLE_LINE_WIDTH
                       and abs(line[0].y - left_edge[0].y) < TABLE_LINE_WIDTH
                       and abs(line[1].y - left_edge[1].y) < TABLE_LINE_WIDTH]
        if len(right_edges) == 0:
            continue  # no matching right edge
        right_edges.sort(key=lambda line: line[0].x)
        right_edge = right_edges[0]
        top_edges = [line
                     for line in merged_horizontal_lines[1]  # "long" horizontal lines
                     if abs(line[0].y - left_edge[0].y) < TABLE_LINE_WIDTH  # aligned with the top of the cell
                     and line[0].x < left_edge[0].x + TABLE_LINE_WIDTH  # including the cell
                     and line[1].x > right_edge[0].x - TABLE_LINE_WIDTH]
        bottom_edges = [line
                        for line in merged_horizontal_lines[1]  # "long" horizontal lines
                        if abs(line[0].y - left_edge[1].y) < TABLE_LINE_WIDTH  # aligned with the bottom of the cell
                        and line[0].x < left_edge[1].x + TABLE_LINE_WIDTH  # including the cell
                        and line[1].x > right_edge[1].x - TABLE_LINE_WIDTH]
        if len(top_edges) == 0 or len(bottom_edges) == 0:
            continue  # no matching top/bottom edge
        cells.append(Rect(left_edge[0], right_edge[1]))

    # regroup contiguous cells (not an equivalence relation, but works for sorted items)
    cells.sort(key=lambda r: (r.top_left.y, r.top_left.x))
    cell_sets = regroup_equivalent_items(cells, are_contiguous)
    if len(cell_sets) == 0:
        return []  # no tables in this page

    # order cells to form tables (list => list of list)
    cell_tables = []
    for cell_set in cell_sets:
        # infer lines based on top edges
        cell_table = []
        count = 0  # total number of cells in lines
        while len(cell_set) > count:
            line_top = cell_set[count].top_left.y
            cell_table.append([
                cell
                for idx, cell in enumerate(cell_set)
                if idx >= count and cell.top_left.y < line_top + TABLE_LINE_WIDTH
            ])
            count = sum([len(line) for line in cell_table])
        cell_tables.append(cell_table)

    # filter out small tables to omit single-cell sections (at least 2x2)
    return [t for t in cell_tables if len(t) > 1 and min(len(line) for line in t) > 1]


def merge_line_overlaps(lines: list[tuple[Point, Point]], horiz: bool) \
        -> tuple[list[tuple[Point, Point]], list[tuple[Point, Point]]]:
    """
    Remove overlaps in the given list of axis-aligned lines, while preserving the overall topology (same covered area).
    Return 2 variants as a pair:
    1. using all the points of the given lines, a set a "short" connected lines with no overlaps
    2. using as few points as possible, a list of "long" lines
    :param lines: a list of lines
    :param horiz: if the lines are horizontal (or else, vertical)
    :return: lists of equivalent lines with no overlap, both short/detailed and long/simplified
    """
    varying_coord_getter = operator.itemgetter(0 if horiz else 1)  # to get the varying coord in lines

    # regroup connected lines (not an equivalence relation, but works for sorted items)
    lines.sort(key=lambda line: (line[0].y, line[0].x))
    connected_lines_sets = regroup_equivalent_items(lines,
                                                    lambda a, b: are_contiguous(Rect(a[0], a[1]), Rect(b[0], b[1]),
                                                                                allow_lines=True, allow_overlaps=True))
    short_lines = []
    long_lines = []
    for connected_lines in connected_lines_sets:
        points = list(set(chain(*[[line[0], line[1]] for line in connected_lines])))
        points.sort(key=varying_coord_getter)
        # short lines use all points (if distant enough)
        for i in range(len(points) - 1):
            if abs(varying_coord_getter(points[i]) - varying_coord_getter(points[i + 1])) >= TABLE_LINE_WIDTH:
                short_lines.append((points[i], points[i + 1]))
        # long lines span all points
        long_lines.append((points[0], points[-1]))
    return short_lines, long_lines


def are_contiguous(r1: Rect, r2: Rect, allow_lines: bool = False, allow_overlaps: bool = False) -> bool:
    """
    Test if the given Rects are contiguous.
    This is true whenever an edge of either one is aligned with, and barely touches, an edge of the other.
    :param r1: a Rect
    :param r2: another Rect
    :param allow_overlaps: if overlapping Rects are allowed
    :param allow_lines: if thin Rects are allowed
    :return: if the given Rects are contiguous
    """
    if not allow_lines:
        for r in [r1, r2]:
            if abs(r.x1 - r.x0) < TABLE_LINE_WIDTH or abs(r.y1 - r.y0) < TABLE_LINE_WIDTH:
                return False  # rect is too thin, cannot be part of a table
    intersect = not (
            r1.br.x < r2.tl.x - TABLE_LINE_WIDTH
            or r1.br.y < r2.tl.y - TABLE_LINE_WIDTH
            or r1.tl.x > r2.br.x + TABLE_LINE_WIDTH
            or r1.tl.y > r2.br.y + TABLE_LINE_WIDTH
    )
    if not intersect:
        return False
    if allow_overlaps:
        return True
    # no overlapping cells means intersection should be thin
    ri = Rect(r1).intersect(r2)
    return ri.width < TABLE_LINE_WIDTH or ri.height < TABLE_LINE_WIDTH


def merge_all_pages(pdf) -> PdfContent:
    """
    Modifies the given PdfContent in place, merging all pages into a new one.
    :param pdf: a root PdfContent with pages
    :return: the new page with all content
    """
    all_pages = [page for page in pdf.find_nodes(NODE_TYPE_PAGE)]

    # add a new page as container
    single_page = PdfContent(name="single-page",
                             parent=pdf,
                             labels={NODE_TYPE_LABEL: NODE_TYPE_PAGE},
                             metadata={"sources": {page.node_name: page.metadata for page in all_pages}})

    # move all children
    for page in all_pages:
        for from_line in page.children:
            shift_nodes(pdf, [from_line.path_name], [f"{single_page.path_name}/{from_line.node_name}"])

    # cleanup
    for page in all_pages:
        shift_nodes(pdf, [page.path_name], [None])

    logging.debug(f"merged {len(all_pages)} pages")
    return single_page


def infer_line_format(pdf: PdfContent):
    """
    Enrich lines metadata in place, with "font" and "size" attributes, based on children spans.
    The font is the most used one; the size is the biggest one.
    :param pdf: some PdfContent
    """
    for line in pdf.find_nodes(NODE_TYPE_LINE):
        line_spans = line.find_nodes(NODE_TYPE_SPAN)
        line.metadata["font"] = most_used_metadata(line_spans, normalize_font)
        line.metadata["size"] = max([normalize_size(span) for span in line.find_nodes(NODE_TYPE_SPAN)])


def most_used_metadata(contents: Iterable[PdfContent], metadata_getter):
    """
    Select one of the most used metadata in the given content.
    :param contents: collection of contents to parse
    :param metadata_getter: how to extract the metadata to compare
    :return: the most used value, or None if none was found
    """
    values_length: dict[int] = {None: 0}
    for content in contents:
        value = metadata_getter(content)
        text_length = len(content.text)
        values_length.setdefault(value, 0)
        values_length[value] += text_length
    main_font_length = max(values_length.values())
    return [k for k, v in values_length.items() if v == main_font_length][0]


def merge_similar_sibling_lines(pdf: PdfContent):
    """
    Modifies the given PdfContent in place,
    merging sibling lines when coming from the same block or having similar format.
    :param pdf: a PdfContent with content to merge
    """
    all_pages = list(pdf.find_nodes(NODE_TYPE_PAGE))
    for page in all_pages:
        # create a new page container
        new_page = PdfContent(name=f"{page.node_name}-merged",
                              parent=pdf,
                              labels={NODE_TYPE_LABEL: NODE_TYPE_PAGE},
                              metadata=page.metadata)

        # mark mergeable lines (to the right)
        mergeable_lines = [
            child.is_typed(NODE_TYPE_LINE) and child.right_sibling is not None
            and child.right_sibling.is_typed(NODE_TYPE_LINE) and same_format(child, child.right_sibling)
            for child in page.children
        ]

        i = -1
        j = 0
        while j < len(mergeable_lines):
            if not mergeable_lines[j]:  # end of mergeable sub-list
                if i < j - 1:
                    source_lines = page.children[0:j - i]
                    # create an aggregating line in the new page
                    new_line = PdfContent(name=f"{source_lines[0].node_name}-{source_lines[-1].node_name}",
                                          parent=new_page,
                                          text=re.sub(" +", " ", " ".join([line.text for line in source_lines])),
                                          labels={NODE_TYPE_LABEL: NODE_TYPE_LINE},
                                          metadata={
                                              "sources": {s.node_name: s.metadata for s in source_lines},
                                              "font": normalize_font(source_lines[0]),
                                              "size": normalize_size(source_lines[0]),
                                          })
                    # migrate children and remove merged lines
                    for source_line in source_lines:
                        for child in source_line.children:
                            shift_nodes(pdf, [child.path_name], [f"{new_line.path_name}/{child.node_name}"])
                        shift_nodes(pdf, [source_line.path_name], [None])
                else:
                    unchanged_line = page.children[0]
                    shift_nodes(pdf, [unchanged_line.path_name], [f"{new_page.path_name}/{unchanged_line.node_name}"])
                i = j
            j += 1

        # cleanup
        shift_nodes(pdf, [page.path_name], [None])


def infer_titles(pdf: PdfContent):
    """
    Modifies the given PdfContent in place, introducing a line hierarchy based on font size.
    Images are always considered a direct subsection of their predecessor node.
    :param pdf: a whole PdfContent
    """

    # no hierarchy below the main font size
    main_font_size = most_used_metadata(pdf.find_nodes(NODE_TYPE_LINE), normalize_size)

    for image in pdf.find_nodes(NODE_TYPE_IMAGE):
        if image.left_sibling is not None and image.left_sibling.is_typed(NODE_TYPE_LINE):
            shift_nodes(pdf, [image.path_name], [f"{image.left_sibling.path_name}/{image.node_name}"])

    all_lines = [line for line in pdf.find_nodes(NODE_TYPE_LINE, NODE_TYPE_TABLE)]
    for line in all_lines:
        while is_subsection(line, main_font_size):
            shift_nodes(pdf, [line.path_name], [f"{line.left_sibling.path_name}/{line.node_name}"])


def is_subsection(line: PdfContent, main_font_size: float) -> bool:
    """
    Tells if the given line is a subsection of its left sibling.
    :param line: a line
    :param main_font_size: the font size titles should exceed
    :return: if the given line should be a subsection of its left sibling
    """
    sibling = line.left_sibling
    if sibling is None or not sibling.is_typed(NODE_TYPE_LINE) or normalize_size(sibling) <= main_font_size:
        return False
    return normalize_size(line) < normalize_size(sibling)


def normalize_size(content: PdfContent) -> (float, None):
    """
    Normalize the given node's size in order to compare values more leniently.
    :param content: some PdfContent
    :return: a comparable, rounded size; or None if not applicable
    """
    raw_size = content.metadata["size"] if "size" in content.metadata else None
    if raw_size is None:
        return None
    return round(float(raw_size) * 4, 0) / 4  # rounded to 0.25


def normalize_font(content: PdfContent) -> (str, None):
    """
    Normalize the given node's font name in order to compare values more leniently.
    :param content: some PdfContent
    :return: a simpler, comparable font name; or None if not applicable
    """
    raw_font = content.metadata["font"] if "font" in content.metadata else None
    if raw_font is None:
        return None
    return re.sub("-(Bold|Italic|Black|Regular)+|MT\\b|PS\\b", "", raw_font)  # ignore modifiers & suffixes


def same_format(content1: PdfContent, content2: PdfContent) -> bool:
    """
    Returns if the given PdfContents have similar text formatting.
    :param content1: some content
    :param content2: another content
    :return: if the given contents are similar, formatting-wise; or None if not comparable
    """
    font1 = normalize_font(content1)
    font2 = normalize_font(content2)
    size1 = normalize_size(content1)
    size2 = normalize_size(content2)
    if font1 is None or font2 is None or size1 is None or size2 is None:
        return False
    same_font = font1 == font2
    same_font_size = abs(size1 - size2) < .5
    return same_font and same_font_size


def to_ascii(image: bytearray) -> str:
    """
    Provides a textual, terminal-compatible view of the given image.
    For debug purposes only.

    :param image: any image
    :return: a colored ASCII-art image
    """
    temp_file = tempfile.NamedTemporaryFile(prefix="unibsv-", delete=False)
    with temp_file:
        temp_file.write(image)
    ascii_art = AsciiArt.from_image(temp_file.name).to_ascii(columns=100)
    os.unlink(temp_file.name)
    return "\n" + ascii_art


def clean_text(content: PdfContent):
    """
    Clean text for further analysis.
    Lower case the text + remove extra spaces.
    :param content: some PdfContent
    """
    for node in findall(content, lambda n: n.text):
        text = node.text.casefold().split()
        node.text_clean = " ".join(text)
