"""
UniBSV module.
Parses french "Bulletin de Santé du Végétal" (BSV) files in PDF format, to generate regional reports on plants health.

Processing is split in 3 phases:
* reading a PDF file as a tree structure with text & images;
* processing this tree to infer knowledge about plants;
* producing a report (writable in CSV format).
"""
