import logging
import sys
import time
from argparse import ArgumentParser
from logging import getLevelName, DEBUG, INFO, WARN
from pathlib import Path

import fitz
from ruamel.yaml import dump, scalarstring, YAML

from unibsv.configuration import get_config, CONFIG_STEMMER, CONFIG_STAGE_LEVELS, CONFIG_STAGE_LEVEL_MAPPING
from unibsv.debug import visualize_blocks, visualize_images
from unibsv.preprocess import read_pdf
from unibsv.process import process, Report, write_report


def process_pdf(input_file_paths: list[str], output_file_path: str = None, config_file: str = None) -> Report:
    """
    Main entrypoint of the unibsv module.
    Parses the given BSV files to produce a report.
    This report is written to the file filesystem if an output path is provided.
    :param input_file_paths: input files in PDF format
    :param output_file_path: (optional) where to write the report, in CSV format
    :param config_file: (optional) configuration override, in YAML format
    :return: the generated report
    """
    report = Report()
    for input_file_path in input_file_paths:
        pdf = read_pdf(input_file_path)
        process(pdf, report, config_file)
    if output_file_path:
        write_report(report, output_file_path)
    return report


if __name__ == "__main__":
    start_time = time.perf_counter()

    parser = ArgumentParser(prog='unibsv',
                            description='Parse regional "Bulletins de Santé du Végétal" (BSV) '
                                        'to generate a report on plants health.',
                            epilog='Sources: https://gitlab.com/data-challenge-gd4h/unibsv/')
    parser.add_argument('input',
                        nargs='*',
                        help='input BSV files, in PDF format')
    parser.add_argument('-o', '--output',
                        help='output report file, in CSV format')
    parser.add_argument('-v', '--verbose',
                        help='more logs (can be repeated)',
                        action='count',
                        default=0)

    # config options
    parser.add_argument('-c', '--config',
                        help='configuration file, in YAML format')
    parser.add_argument('--show-config',
                        action="store_true",
                        help='just show the current configuration and exit')

    # dev/debug options
    parser.add_argument('--debug-blocks',
                        help='(debug) render the PDF pages with blocks in the given directory',
                        metavar="DIR")
    parser.add_argument('--debug-images',
                        help='(debug) render the processed images in the given directory',
                        metavar="DIR")

    args = parser.parse_args()

    # show help when no args
    if not any(vars(args).values()):
        parser.print_help()
        exit(0)

    # allow dirs as source
    args_input = [
        Path(file).glob("**/*") if Path(file).is_dir()
        else [file]
        for file in args.input
    ]
    # accept *.pdf files only
    args_input = [str(file) for files in args_input for file in files if str(file).lower().endswith(".pdf")]

    # handle --show-config first, to output raw YAML (useful to initialize an alt config)
    if args.show_config:
        config = get_config(args.config)
        # omit unprintable keys
        config.pop(CONFIG_STAGE_LEVELS)
        config.pop(CONFIG_STAGE_LEVEL_MAPPING)
        config.pop(CONFIG_STEMMER)
        # handle multi-lines nicely
        scalarstring.walk_tree(config)
        # do pretty-print
        yaml = YAML()
        yaml.allow_unicode=True
        yaml.default_flow_style=False
        yaml.dump(config, sys.stdout)
        exit(0)

    # logging setup
    log_level = DEBUG if args.verbose > 1 else INFO if args.verbose > 0 else WARN
    logging.basicConfig(level=log_level)
    logging.info(f"Verbosity: {getLevelName(log_level)}")

    # handle --debug-blocks option
    if args.debug_blocks:
        logging.info(f"Rendering blocks in directory {args.debug_blocks}")
        for input_file in args_input:
            with fitz.open(input_file) as doc:
                images = visualize_blocks(doc, args.debug_blocks)
                logging.debug(f"printed images {images}")

    # handle --debug-images option
    if args.debug_images:
        logging.info(f"Rendering images in directory {args.debug_images}")
        for input_file in args_input:
            with fitz.open(input_file) as doc:
                visualize_images(input_file, args.debug_images)

    # process the BSV into a report
    report = process_pdf(args_input, output_file_path=args.output, config_file=args.config)

    # dump the source tree in the console, based on verbosity
    if args.verbose > 1:
        log_format = "{0:full}" if args.verbose > 3 else "{0}" if args.verbose > 2 else "{0:short}"
        logging.info(f"Drawing {len(report.pdf_trees)} source PDF tree(s):\n" +
                     "\n".join([log_format.format(pdf_tree) for pdf_tree in report.pdf_trees]))

    elapsed_time = time.perf_counter() - start_time
    logging.info(f"Processed {len(args_input)} file(s) in {elapsed_time:1.0f}s")
