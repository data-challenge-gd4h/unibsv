import os

from bigtree import findall

from unibsv.preprocess import read_pdf, NODE_TYPE_IMAGE
from unibsv.process import process_image

pdf = read_pdf("../data/UniBSV/BSV PACA/BSVViti_20220517_N9-2_cle44c398.pdf")

print("{0:short}".format(pdf))

regions = [
    "Alsace",
    "PACA",
    "Auvergne-Rhône-Alpes",
    "Champagne",
    "Nouvelle-Aquitaine"
]

input_folder = "../data/UniBSV"
output_folder = "../data/UniBSV/processed_scale_images/"

paths = [os.path.join(input_folder, f"BSV {region}") for region in regions]

for region in paths:
    pdf_files = [pdf for pdf in os.listdir(region) if pdf.endswith(".pdf")][:2]
    for pdf in pdf_files:
        pdf_path = os.path.join(region, pdf)
        pdf = read_pdf(os.path.join(region, pdf))

        for i, node in enumerate(findall(pdf, lambda n: n.is_typed(NODE_TYPE_IMAGE))):
            if node.image:
                risk = process_image(node, i, output_folder)
                if risk:
                    print(risk)
