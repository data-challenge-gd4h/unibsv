import os.path
import re
from pathlib import Path

from fitz import Rect, Point
from pandas import DataFrame

from unibsv.preprocess import detect_tables, merge_line_overlaps, read_pdf, NODE_TYPE_TABLE


class MockFitzPage:
    def __init__(self, rects: list[Rect]):
        self.drawings = [{"rect": r} for r in rects]

    def get_drawings(self):
        return self.drawings


class TestTables:
    """
    Tests about table handling in source PDF files.
    """

    def test_detect_tables_with_one_2x3_table_drawn_as_rects(self):
        # Arrange a page with a 2x3 table
        page = MockFitzPage([
            Rect(0, 0, 10, 10),
            Rect(0, 10, 10, 20),
            Rect(10, 0, 20, 10),
            Rect(10, 10, 20, 20),
            Rect(20, 0, 30, 10),
            Rect(20, 10, 30, 20),
        ])

        # Act
        tables = detect_tables(page)

        # Assert 1 table with 2 lines, 3 columns
        assert len(tables) == 1
        assert tables[0] == [[
            Rect(0, 0, 10, 10),
            Rect(10, 0, 20, 10),
            Rect(20, 0, 30, 10),
        ], [
            Rect(0, 10, 10, 20),
            Rect(10, 10, 20, 20),
            Rect(20, 10, 30, 20),
        ]]

    def test_detect_tables_with_one_2x3_table_drawn_as_lines(self):
        # Arrange a page with a 2x3 table
        page = MockFitzPage([
            Rect(0, 0, 10, 0), Rect(0, 0, 1, 10), Rect(10, 0, 10, 10), Rect(0, 10, 10, 10),
            Rect(0, 10, 10, 11), Rect(0, 10, 1, 20), Rect(10, 10, 11, 20), Rect(0, 19, 10, 20),
            Rect(10, 0, 20, 1), Rect(10, 0, 11, 10), Rect(20, 0, 20, 10), Rect(10, 10, 20, 10),
            Rect(10, 10, 20, 11), Rect(10, 10, 11, 20), Rect(20, 10, 20, 20), Rect(10, 19, 20, 20),
            Rect(20, 0, 30, 1), Rect(20, 0, 21, 10), Rect(30, 0, 31, 10), Rect(20, 10, 30, 11),
            Rect(20, 10, 30, 11), Rect(20, 10, 21, 20), Rect(30, 10, 30, 20), Rect(20, 19, 30, 20),
        ])

        # Act
        tables = detect_tables(page)

        # Assert 1 table with 2 lines, 3 columns
        assert len(tables) == 1
        assert tables[0] == [[
            Rect(0, 0, 10, 10),
            Rect(10, 0, 20, 10),
            Rect(20, 0, 30, 10),
        ], [
            Rect(0, 10, 10, 20),
            Rect(10, 10, 20, 20),
            Rect(20, 10, 30, 20),
        ]]

    def test_detect_tables_with_many_tables(self):
        # Arrange a page with 3 tables
        sample = [
            Rect(0, 0, 10, 10),
            Rect(0, 10, 10, 20),
            Rect(10, 0, 20, 10),
            Rect(10, 10, 20, 20),
            Rect(20, 0, 30, 10),
            Rect(20, 10, 30, 20),
        ]
        page = MockFitzPage(sample + [r + 100 for r in sample] + [r + 300 for r in sample])

        # Act
        tables = detect_tables(page)

        # Assert 3 tables
        assert len(tables) == 3

    def test_merge_line_overlaps_with_vertical_lines(self):
        # Arrange a set of lines with overlaps
        lines = [
            (Point(0, 0), Point(0, 10)),
            (Point(100, 5), Point(100, 15)),
            (Point(200, 5), Point(200, 15)),
            (Point(0, 10), Point(0, 20)),
            (Point(0, 20), Point(0, 30)),
            (Point(100, 15), Point(100, 25)),
            (Point(0, 15), Point(0, 25)),
        ]

        # Act
        processed_lines = merge_line_overlaps(lines, False)

        # Assert "short" lines are correct
        assert sorted(processed_lines[0], key=lambda o: f"{o}") == sorted({
            (Point(0, 0), Point(0, 10)),
            (Point(0, 10), Point(0, 15)),
            (Point(0, 15), Point(0, 20)),
            (Point(0, 20), Point(0, 25)),
            (Point(0, 25), Point(0, 30)),
            (Point(100, 5), Point(100, 15)),
            (Point(100, 15), Point(100, 25)),
            (Point(200, 5), Point(200, 15)),
        }, key=lambda o: f"{o}")
        # Assert "long" lines are correct
        assert sorted(processed_lines[1], key=lambda o: f"{o}") == sorted({
            (Point(0, 0), Point(0, 30)),
            (Point(100, 5), Point(100, 25)),
            (Point(200, 5), Point(200, 15)),
        }, key=lambda o: f"{o}")

    def test_read_pdf_with_complex_tables(self):
        # Arrange a PDF page with complex tables (2 tables, with both lines and colored cells, empty cells, etc.)
        pdf_file = os.path.join(Path(__file__).parent, "tables-01.pdf")

        # Act
        pdf = read_pdf(pdf_file)

        # Assert all tables are correctly read and placed in the tree
        tables = pdf.find_nodes(NODE_TYPE_TABLE)
        assert len(tables) == 2

        assert tables[0].parent.text.strip() == "Stades phénologiques"
        assert format_table(tables[0].table) == format_table(DataFrame([
            ("Cépages", "Le plus tardif", "Majoritaire", "Le plus avancé"),
            ("Chardonnay", "baies de la taille d'un pois, les grappes pendent (31)", "les baies se touchent (31-33)",
             "fermeture de la grappe (33)"),
            ("Gamay", "baies de la taille d'un pois, les grappes pendent (31)", "les baies se touchent (31-33)",
             "fermeture de la grappe (33)"),
            ("Grenache N", "baies de la taille d'un grain de plomb (29)", "premières baies taille pois (29-31)",
             "baies de la taille d'un pois, les grappes pendent (31)"),
            ("Merlot", "premières baies taille pois (29-31)", "baies de la taille d'un pois, les grappes pendent (31)",
             "les baies se touchent (31-33)"),
            ("Syrah (2 parcelles)", "", "baies de la taille d'un pois, les grappes pendent (31)", ""),
        ]))

        assert tables[1].parent.text.strip() == "Oïdium"
        assert format_table(tables[1].table) == format_table(DataFrame([
            ("NOMBRE GRAPPES ATTEINTES / 100 G", "Nombre de parcelles notées 15", "% parcelles (sur parcelles notées)"),
            ("Absence", "8", "53"),
            ("0-5", "6", "40"),
            ("5-20", "0", "0"),
            (">20", "1", "7"),
        ]))

    def test_read_pdf_with_borderless_table(self):
        # Arrange a PDF page with a table with no border (colored cells)
        pdf_file = os.path.join(Path(__file__).parent, "tables-02.pdf")

        # Act
        pdf = read_pdf(pdf_file)

        # Assert the table is correctly read and placed in the tree
        tables = pdf.find_nodes(NODE_TYPE_TABLE)
        assert len(tables) == 1
        assert tables[0].parent.text.strip() == "Grenache"
        assert format_table(tables[0].table) == format_table(DataFrame([
            ("Secteur 0", "Secteur I", "Secteur II", "Secteur III", "Secteur IV", "Secteur V"),
            ("Stade 6-7 à 13- 14 8-10 feuilles étalées majoritaire",
             "Stade 5-6 à 8- 10 feuilles. 7-9 feuilles étalées majoritaire",
             "Stade 4-5 à 8-9 feuilles. 6-8 feuilles étalées majoritaire",
             "Stade 2-3 à 6-7 feuilles 5-6 feuilles étalées majoritaire",
             "Stade D à 5-6 feuilles 4-5 feuilles majoritaire",
             "Stade 2-3 feuilles étalées majoritaire")
        ]))

    def test_read_pdf_with_tables_with_text_spanning_many_cells_or_being_very_long(self):
        # Arrange a PDF page with interesting tables
        pdf_file = os.path.join(Path(__file__).parent, "tables-03.pdf")

        # Act
        pdf = read_pdf(pdf_file)

        # Assert the tables are correctly read and placed in the tree
        tables = pdf.find_nodes(NODE_TYPE_TABLE)
        assert len(tables) == 2

        assert "Modélisation (source IFV) réalisée le 28/06/2022 (J)" == tables[0].parent.text.strip()
        assert format_table(tables[0].table) == format_table(DataFrame([
            ("Hypothèse météorologique", "28/06", "29/06", "30/06", "01/07"),  # 1 line in source PDF
            ("H1", "0", "0.4", "2.7", "0"),
            ("H2", "0.2", "3.1", "8.3", "0"),
            ("H3", "0.8", "9.4", "13.1", "0.5"),
        ]))

        sibling = "Les deux hypothèses météorologiques H1 et H3 n’ont que 10 % de chance d’être dépassées et" \
                  " constituent une limite à la zone d’incertitude due à la prévision météorologique."
        assert tables[1].left_sibling.text.strip() == sibling
        assert tables[1].table.values[0][0] == "Situation de J-7 à J"
        assert tables[1].table.values[0][1] == "Simulation de J à J+3"
        assert re.fullmatch("Au cours de la semaine dernière, .+ pour atteindre un total de 10 %\\.",
                            tables[1].table.values[1][0])
        assert re.fullmatch("Dans les trois jours à venir, .+ avec une hausse moyenne de la FTA de \\+10 points\\.",
                            tables[1].table.values[1][1])

    def broken_test_read_pdf_with_table_having_contiguous_unrelated_rect(self):
        # Arrange a PDF page with a table having parasitic rect
        pdf_file = os.path.join(Path(__file__).parent, "tables-04.pdf")

        # Act
        pdf = read_pdf(pdf_file)

        # Assert the table is correctly read and placed in the tree
        tables = pdf.find_nodes(NODE_TYPE_TABLE)
        assert len(tables) == 1
        assert tables[0].parent.text.strip() == "Grenache"
        assert format_table(tables[0].table) == format_table(DataFrame([
            ("Secteur 0", "Secteur I", "Secteur II", "Secteur III", "Secteur IV", "Secteur V"),
            (
                "Fin véraison majoritaire",
                "Fin véraison majoritaire",
                "75% véraison majoritaire",
                "50% véraison majoritaire",
                "25% véraison majoritaire",
                "Première baies vérées majoritaire",
            ),
            (
                "Stade 75% véraison à fin véraison",
                "Stade 50% véraison à fin véraison",
                "Stade 50% à 75% véraison",
                "Stade début véraison à 75% véraison",
                "Stade fermeture de la grappe à 75% véraison",
                "Stade fermeture de la grappe à 25% véraison",
            ),
        ]))

    def broken_test_read_pdf_with_table_having_merged_cells(self):
        # Arrange a PDF page with a table having merged cells
        pdf_file = os.path.join(Path(__file__).parent, "tables-05.pdf")

        # Act
        pdf = read_pdf(pdf_file)

        # Assert the table is correctly read and placed in the tree
        tables = pdf.find_nodes(NODE_TYPE_TABLE)
        assert len(tables) == 1
        assert tables[0].parent.text.strip() == "STADES VEGETATIFS"
        assert format_table(tables[0].table) == format_table(DataFrame([
            ("Vignoble", "Stades Mini", "Stades Maxi"),
            ("Saône et Loire", "1 feuille étalée", "4-5 feuilles étalées"),
            ("Côte d’Or", "1 feuille étalée", "4-5 feuilles étalées"),
            ("Yonne", "1 feuille étalée", "3-4 feuilles étalées"),
            ("Nièvre", "1 feuille étalée", "3-4 feuilles étalées"),
            ("Franche-Comté", "Eclatement du bourgeon", "5-6 feuilles étalées"),
        ]))


def format_table(df: DataFrame) -> str:
    return df.to_csv(sep='|', lineterminator="\n", index=False, header=False)
