import datetime
import os
import tempfile

import pandas
from pandas import DataFrame

from unibsv.configuration import get_config
from unibsv.process import Report, write_report, CSV_HEADERS, resolve_stages_from_table


class TestCsv:
    """
    Tests about CSV-formatted reports.
    """

    def test_write_report_with_no_data(self):
        # Arrange a report with no data
        report = Report()

        # Act
        temp_file = tempfile.NamedTemporaryFile(prefix="test-", suffix=".csv", delete=False)
        write_report(report, temp_file.name)

        # Assert the file was written without error
        assert os.path.isfile(temp_file.name)
        csv = pandas.read_csv(temp_file.name, sep=";")
        assert list(csv.columns) == CSV_HEADERS
        assert len(csv.values) == 0

    def test_write_report_with_stages(self):
        # Arrange a report with data
        report = Report()
        # 1 BSV with both phenological stages and pest (with secondary pest as well)
        report.add_stage(source="file1", region="A1", zone="B1", date=datetime.date(2023, 4, 30),
                         stage_most="C1", stage_mini="D1", stage_maxi="E1")
        report.add_risk(source="file1", region="A1", zone="B1", date=datetime.date(2023, 4, 30),
                        pest="F1", risk_level="G1")
        report.add_risk(source="file1", region="A1", zone="B1", date=datetime.date(2023, 4, 30),
                        pest="F2")
        # 1 BSV with stages only
        report.add_stage(source="file2", region="A2", zone="B2", date=datetime.date(2023, 4, 29),
                         stage_most="C2", stage_mini="D2", stage_maxi="E2")
        # 1 BSV with pest only
        report.add_risk(source="file3", region="A3", zone="B3", date=datetime.date(2023, 4, 28),
                        pest="F3")

        # Act
        temp_file = tempfile.NamedTemporaryFile(prefix="test-", suffix=".csv", delete=False)
        write_report(report, temp_file)

        # Assert the file was written with the report data
        assert os.path.isfile(temp_file.name)
        csv = pandas.read_csv(temp_file.name, sep=";").fillna("")
        assert list(csv.columns) == CSV_HEADERS
        csv_rows = [[v for v in r] for r in csv.values]
        assert all(row in csv_rows for row in [
            ['file1', 'A1', 'B1', '2023-04-30', 'C1', 'D1', 'E1', 'F1', 'G1'],
            ['file1', 'A1', 'B1', '2023-04-30', 'C1', 'D1', 'E1', 'F2', ''],
            ['file2', 'A2', 'B2', '2023-04-29', 'C2', 'D2', 'E2', '', ''],
            ['file3', 'A3', 'B3', '2023-04-28', '', '', '', 'F3', ''],
        ])


class TestPhenologicalStagesFromTablesNoHeaders:
    """
    Tests about phenological stages extraction from tables with no headers (PACA).
    """

    def test_resolve_stages_from_table_based_on_2_cells(self):
        # Arrange a table of stages (PACA 2022-06-14)
        table = DataFrame([
            ("Secteur 0", "Baie 10-12 mm majoritaire", "Stade baie 6-7 mm à fermeture de la grappe"),
            ("Secteur I", "Baie 8-9 mm majoritaire", "Stade 4-5 mm à fermeture de la grappe"),
            ("Secteur II", "Baie 7-8 mm majoritaire", "Stade 2-3 à 9-10 mm"),
            ("Secteur III", "Baie 5-7 mm majoritaire", "Stade 2-3 à 7-8 mm"),
            ("Secteur IV", "Baie 3-5 mm majoritaire", "Stade nouaison à 5-6 mm"),
            ("Secteur V", "Stade 2-3 mm majoritaire", "Stade pleine floraison à 3-4 mm"),
        ]).transpose()  # zones as headers

        # Act
        stages = resolve_stages_from_table(table, get_config())

        # Assert data is extracted as expected (most/min/max tuples)
        assert len(stages) == 6
        assert stages.get("Secteur 0") == ("Baie 10-12 mm majoritaire", "Stade baie 6-7 mm", "fermeture de la grappe")
        assert stages.get("Secteur I") == ("Baie 8-9 mm majoritaire", "Stade 4-5 mm", "fermeture de la grappe")
        assert stages.get("Secteur II") == ("Baie 7-8 mm majoritaire", "Stade 2-3", "9-10 mm")
        assert stages.get("Secteur III") == ("Baie 5-7 mm majoritaire", "Stade 2-3", "7-8 mm")
        assert stages.get("Secteur IV") == ("Baie 3-5 mm majoritaire", "Stade nouaison", "5-6 mm")
        assert stages.get("Secteur V") == ("Stade 2-3 mm majoritaire", "Stade pleine floraison", "3-4 mm")

    def test_resolve_stages_from_table_based_on_1_short_cell(self):
        # Arrange a table of stages (PACA 2022-08-17)
        table = DataFrame([
            ("Secteur II", "Début Récolte"),
            ("Secteur III", "100% véraison"),
            ("Secteur IV", "50% à fin véraison"),
            ("Secteur V", "50% à 75% véraison"),
        ]).transpose()  # zones as headers

        # Act
        stages = resolve_stages_from_table(table, get_config())

        # Assert data is extracted as expected (most/min/max tuples)
        assert len(stages) == 4
        assert stages.get("Secteur II") == ("Début Récolte", "Début Récolte", "Début Récolte")
        assert stages.get("Secteur III") == ("100% véraison", "100% véraison", "100% véraison")
        assert stages.get("Secteur IV") == ("50% à fin véraison", "50%", "fin véraison")
        assert stages.get("Secteur V") == ("50% à 75% véraison", "50%", "75% véraison")

    def test_resolve_stages_from_table_based_on_1_long_cell(self):
        # Arrange a table of stages (PACA 2022-04-05)
        table = DataFrame([
            ("Secteur 0", "Stade C à F D majoritaire"),
            ("Secteur I", "Stade C à E C/D majoritaires"),
            ("Secteur II", "Stade A à D B/C majoritaire"),
            ("Secteur III", "Stade A à C A/B majoritaire"),
            ("Secteur IV", "Stade A à B A majoritaire"),
            ("Secteur V", "Stade A"),
        ]).transpose()  # zones as headers

        # Act
        stages = resolve_stages_from_table(table, get_config())

        # Assert data is extracted as expected (most/min/max tuples)
        assert len(stages) == 6
        assert stages.get("Secteur 0") == ("D majoritaire", "Stade C", "F")
        assert stages.get("Secteur I") == ("C/D majoritaires", "Stade C", "E")
        assert stages.get("Secteur II") == ("B/C majoritaire", "Stade A", "D")
        assert stages.get("Secteur III") == ("A/B majoritaire", "Stade A", "C")
        assert stages.get("Secteur IV") == ("A majoritaire", "Stade A", "B")
        assert stages.get("Secteur V") == ("Stade A", "Stade A", "Stade A")


class TestPhenologicalStagesFromTablesWithHeaders:
    """
    Tests about phenological stages extraction from tables with headers (RA, BFC).
    """

    def test_resolve_stages_from_table_with_all_headers(self):
        # Arrange a table of stages (RA 2022-04-26 / Diois)
        table = DataFrame(data=[
            ("Cépages", "Le plus tardif", "Majoritaire", "Le plus avancé"),
            ("Aligoté (1 parcelle suivie)", "", "1ère feuille étalée (07)", ""),
            ("Muscat", "Eclatement du bourgeon (06)", "1ère feuille étalée (07)", "5-6 feuilles étalées (12)"),
            ("Clairette", "Début éclatement du bourgeon (03-05)", "Apparition des feuilles (06-07)",
             "2-3 feuilles étalées (09)"),
        ])

        # Act
        stages = resolve_stages_from_table(table, get_config())

        # Assert data is extracted as expected (most/min/max tuples)
        assert len(stages) == 3
        assert stages.get("Aligoté (1 parcelle suivie)") == (
            "1ère feuille étalée (07)",
            "1ère feuille étalée (07)",  # filled using "most" stage
            "1ère feuille étalée (07)",  # filled using "most" stage
        )
        assert stages.get("Muscat") == (
            "1ère feuille étalée (07)",
            "Eclatement du bourgeon (06)",
            "5-6 feuilles étalées (12)"
        )
        assert stages.get("Clairette") == (
            "Apparition des feuilles (06-07)",
            "Début éclatement du bourgeon (03-05)",
            "2-3 feuilles étalées (09)"
        )

    def test_resolve_stages_from_table_with_some_headers(self):
        # Arrange a table with only zone and min/max stage columns (BFC 2022-05-10)
        table = DataFrame(data=[
            ("Vignoble", "Stades Mini", "Stades Maxi"),
            ("Saône et Loire", "3-4 feuilles étalées", "11-12 feuilles étalées"),
            ("Côte d’Or", "4 feuilles étalées", "9 feuilles étalées"),
            ("Yonne", "2-3 feuilles étalées", "6-7 feuilles étalées"),
            ("Nièvre", "4-5 feuilles étalées", "6-7 feuilles étalées"),
            ("Franche-Comté", "3-4 feuilles étalées", "8-9 feuilles étalées"),
        ])

        # Act
        stages = resolve_stages_from_table(table, get_config())

        # Assert data is extracted as expected (most/min/max tuples with min/max only)
        assert len(stages) == 5
        assert stages.get("Saône et Loire") == (None, "3-4 feuilles étalées", "11-12 feuilles étalées")
        assert stages.get("Côte d’Or") == (None, "4 feuilles étalées", "9 feuilles étalées")
        assert stages.get("Yonne") == (None, "2-3 feuilles étalées", "6-7 feuilles étalées")
        assert stages.get("Nièvre") == (None, "4-5 feuilles étalées", "6-7 feuilles étalées")
        assert stages.get("Franche-Comté") == (None, "3-4 feuilles étalées", "8-9 feuilles étalées")
