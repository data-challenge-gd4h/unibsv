# Challenge GD4H - UniBSV

*For an english translated version of this file, follow this [link](/README.en.md)*

Le <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) est une offre de service incubée au sein de l’ECOLAB, laboratoire d’innovation pour la transition écologique du Commissariat Général au Développement Durable.

Dans ce cadre, un challenge permettant le développement d’outils ancrés dans la communauté de la donnée en santé-environnement afin d’adresser des problématiques partagées a été organisé en 2023.

<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Site</a>

## UniBSV

Les personnes vivant près de cultures sont susceptibles d’être exposées aux produits phytopharmaceutiques (PPP) utilisés lors des traitements.
La viticulture est l’une des cultures utilisant le plus de PPP : 14% des dépenses de pesticides pour seulement 3% de surface cultivée. Pour conduire des études (comme PestiRiv) sur le lien entre la proximité de l’habitat et la surexposition aux PPP et ses conséquences, la prise en compte des usages des PPP dans les cultures devrait être effectuée.

Dans chaque région, des Bulletins de Santé des Végétaux (BSV) sont produits pour aider les agriculteurs à combattre des bioagresseurs, et sont donc un bon proxy de l’usage de PPP.

<a href="https://gd4h.ecologie.gouv.fr/defis/804" target="_blank" rel="noreferrer">En savoir plus sur le défi</a>

## **Documentation**

Cet outil propose d'analyser un ou plusieurs BSV au format PDF et d'en extraire des informations utiles : stades phénologiques, bioagresseurs et risque associé.
Il compile ces informations dans un document au format CSV.

L'analyse d'un BSV comporte deux phases:
* lecture du PDF, format orienté rendu, pour reconstituer un texte structuré en sections et comportant des tableaux et des images;
* analyse du contenu, pour sélectionner les éléments pertinents et en extraire les informations recherchées.

Le format de sortie comporte :
* des metadata sur le BSV (nom de fichier, région, date)
* la phénologie (stade majoritaire, le plus tardif, le plus avancé)
* les bioagresseurs (dénomination, risque associé)

### **Installation**

[Guide d'installation](/INSTALL.md)

### **Utilisation**

Le module Python `unibsv` s'exécute en ligne de commande (CLI).
Il peut être exécuté à partir des sources ou en tant que module installé localement (via [pip](https://pypi.org/project/pip/)) ou en tant que binaire compilé (voir le [guide d'installation](/INSTALL.md)).
Dans les exemples qui suivent, on écrira `unibsv` pour désigner abstraitement n'importe laquelle de ces options (`python -m unibsv`, `unibsv.exe`, etc.).

L'usage le plus simple en est le suivant :
```shell
unibsv bsv.pdf --output output.csv
```
Le fichier `bsv.pdf` est ici un BSV et le résultat de son analyse sera écrit dans un fichier `output.csv` (nouveau ou écrasé).
Tout se passe dans le répertoire courant, mais des chemins absolus peuvent être donnés.

Les différentes options (plus de logs, plus d'explications sur l'analyse du PDF et de ses tableaux ou images) sont détaillées dans la sortie console avec l'option `-h`/`--help`.

#### Exemples pratiques

Analyser tout un répertoire de BSV (ici, `data/UniBSV`) en incluant récursivement tous les sous-répertoires :
```shell
unibsv data/UniBSV -o output.csv
```

Analyser plusieurs BSV, unitairement (`bsv1.pdf` et `bsv2.pdf`) ou regroupés en répertoires (`/tmp/Alsace` et `/tmp/PACA`) :
```shell
unibsv bsv1.pdf bsv2.pdf /tmp/Alsace /tmp/PACA -o output.csv
```

Visualiser la structure d'un BSV (blocs, lignes, dessins) dans des rendus du PDF (une image par page) ici écrits dans un répertoire `output/directory` :
```shell
unibsv bsv.pdf --debug-blocks output/directory
```

Visualiser les analyses des jauges dans des BSV, sous forme d'images annotées, ici écrites dans un répertoire `output/directory` :
```shell
unibsv bsv.pdf --debug-images output/directory
```

Modifier la configuration utilisée pour analyser un BSV :
```shell
unibsv bsv.pdf -o output.csv --config custom_config.yaml
```

Initialiser un fichier de configuration pour l'éditer facilement :
```shell
unibsv --show-config > custom_config.yaml
```

### **Contributions**

Si vous souhaitez contribuer à ce projet, merci de suivre les [recommendations](/CONTRIBUTING.md).

### **Licence**

Le code est publié sous licence [MIT](/licence.MIT).

Les données référencés dans ce README et dans le guide d'installation sont publiés sous [Etalab Licence Ouverte 2.0](/licence.etalab-2.0).
