# Installation Guide

## Data Collection

All useful resources can be found [here](https://nx36784.your-storageshare.de/s/B2SRrH6pdnHmsiz):
- documentation about the UniBSV challenge
- sample datasets (BSV files from various regions)

To download everything into your working copy:
```shell
make download_data
```

## Dependencies

The following are required:
- [Python 3.11](https://www.python.org/downloads/) (or more recent)
- [pip](https://pip.pypa.io/en/stable/installation/)

The following are optional:
- [virtualenv](https://virtualenv.pypa.io/en/stable/installation.html) (to setup a Python virtual environment)
- [make](https://www.gnu.org/software/make/#download) (to use the [Makefile](./Makefile))
- [wget](https://www.gnu.org/software/wget/) (to download datasets using the [Makefile](./Makefile))

## Development

To run the solution in development mode locally:

* setup a Python [virtual environment](https://packaging.python.org/en/latest/tutorials/installing-packages/#creating-and-using-virtual-environments)
```shell
virtualenv ~/.local/share/virtualenvs/unibsv
source ~/.local/share/virtualenvs/unibsv/bin/activate
```
* download dependencies:
```shell
pip install -r requirements.txt
```
* run from either the command line:
```shell
cd src
python -m unibsv -h
```
* or from your favourite IDE, using the [\_\_main__.py](src/unibsv/__main__.py) script.

## Production

To run the solution in production mode:

* generate a stand-alone `dist/unibsv` executable file:
```shell
make build_exec
```
* copy the generated binary in your PATH, or execute it directly:
```shell
dist/unibsv -h
```
