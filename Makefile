
PYTHON_CMD ?= python3
PYTEST_CMD ?= pytest

.PHONY: help
help: ## Display this help.
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_0-9-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

.PHONY: clean
clean:  ## Remove all generated files.
	rm dist/ src/*.egg-info/ build/ *.spec -rf

.PHONY: test
test:  ## Run automated tests.
	$(PYTEST_CMD)

.PHONY: install
install: test  ## Install unibsv module locally.
	$(PYTHON_CMD) -m pip install .

.PHONY: build_mod
build_mod: test  ## Build unibsv module archive.
	$(PYTHON_CMD) -m pip install --upgrade build
	$(PYTHON_CMD) -m build

.PHONY: build_exec
build_exec: test  ## Build unibsv as an executable binary file.
	$(PYTHON_CMD) -m pip install --upgrade pyinstaller
	pyinstaller src/unibsv/__main__.py --onefile --name unibsv

.PHONY: download_requirements
download_requirements:  ## Fetch pip dependencies, based on the requirement.txt file.
	$(PYTHON_CMD) -m pip install -r ./requirements.txt

.PHONY: download_data
download_data:  ## Fetch sample datasets and documentation.
	mkdir -p data
	test -f data/UniBSV.zip || wget "https://nx36784.your-storageshare.de/s/B2SRrH6pdnHmsiz/download" --no-clobber -O data/UniBSV.zip
	cd data && unzip -o -q UniBSV.zip
