The <a href="https://gd4h.ecologie.gouv.fr/" target="_blank" rel="noreferrer">Green Data for Health</a> (GD4H) is an initiative led by the Innovation Lab (Ecolab) of the French ministry of ecology.

A challenge has been organised in 2023 to develop tools, rooted in the health-environment data community, aiming at addressing shared issues.

<a href="https://gd4h.ecologie.gouv.fr/defis" target="_blank" rel="noreferrer">Site</a>

## UniBSV

People living near crop fields are likely to be exposed to the plant protection products (PPPs) used during treatments.
Winegrowing is one of the agricultural sectors that uses the most PPPs, accounting for 14% of pesticide expenditure for just 3% of the cultivated area. In order to conduct studies (such as PestiRiv) on the link between proximity to housing and overexposure to PPPs and its consequences, the uses of PPPs in cultivation should be taken into account.

In each region, Bulletins de Santé des Végétaux (BSV - Plant Health Bulletins) are produced to help farmers combat bioaggressors, and are therefore a good proxy for PPP use.

<a href="https://challenge.gd4h.ecologie.gouv.fr/defi/?topic=36" target="_blank" rel="noreferrer">Find out more about the challenge</a>

## **Documentation**

This tool enables you to analyse one or more BSVs in PDF format and extract useful information: phenological stages, pests and associated risks.
It compiles this information in a document in CSV format.

There are two stages in analysing a BSV:
* reading the PDF, a rendering-oriented format, to reconstitute a text structured into sections and including tables and images;
* content analysis, to select the relevant elements and extract the required information.

The output format includes :
* BSV metadata (file name, region, date)
* phenology (majority, latest, most advanced stage)
* pests (name, associated risk)

### **Installation**

[Installation guide](/INSTALL.md)

### **Use**

The `unibsv` Python module runs on the command line (CLI).
It can be run from source or as a locally installed module (via [pip](https://pypi.org/project/pip/)) or as a compiled binary (see the [installation guide](/INSTALL.md)).
In the following examples, `unibsv` is used as an abstract name for any of these options (`python -m unibsv`, `unibsv.exe`, etc.).

The simplest usage is as follows:
```shell
unibsv bsv.pdf --output output.csv
```
The `bsv.pdf` file here is a BSV and the result of its analysis will be written to an `output.csv` file (new or overwritten).
Everything happens in the current directory, but absolute paths can be given.

The various options (more logs, more explanations of the analysis of the PDF and its tables or images) are detailed in the console output with the `-h`/`--help` option.

#### Practical examples

Scan an entire BSV directory (here, `data/UniBSV`) recursively including all sub-directories:
```shell
unibsv data/UniBSV -o output.csv
```

Analyse several BSVs, individually (`bsv1.pdf` and `bsv2.pdf`) or grouped together in directories (`/tmp/Alsace` and `/tmp/PACA`):
```shell
unibsv bsv1.pdf bsv2.pdf /tmp/Alsace /tmp/PACA -o output.csv
```

View the structure of a BSV (blocks, lines, drawings) in PDF renderings (one image per page) written here in an `output/directory` directory:
```shell
unibsv bsv.pdf --debug-blocks output/directory
```

View gauge analyses in BSVs, in the form of annotated images, here written to an `output/directory` directory:
```shell
unibsv bsv.pdf --debug-images output/directory
```

Modify the configuration used to analyse a BSV:
```shell
unibsv bsv.pdf -o output.csv --config custom_config.yaml
```

Initialise a configuration file for easy editing:
```shell
unibsv --show-config > custom_config.yaml
```

### **Contributions**

If you would like to contribute to this project, please follow the [recommendations](/CONTRIBUTING.md).

### **Licence**

The code is released under the [MIT](/licence.MIT) licence.

The data referenced in this README and in the installation guide are published under the [Etalab Open License 2.0](/licence.etalab-2.0).
